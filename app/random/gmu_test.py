import tensorflow as tf
tf.disable_v2_behavior()

import numpy as np
import matplotlib.pyplot as plt

if __name__ == '__main__':
    np.random.seed(41)
    tf.set_random_seed(41)

    n = 400
    p_c = 0.5
    p_m = 0.5
    mu_v_0 = 1.0
    mu_v_1 = 8.0
    mu_v_noise = 17.0
    mu_t_0 = 13.0
    mu_t_1 = 19.0
    mu_t_noise = 10.0

    c = np.random.binomial(n=1, p=p_c, size=n)
    m = np.random.binomial(n=1, p=p_m, size=n)
    y_v = np.random.randn(n) + np.where(c == 0, mu_v_0, mu_v_1)
    y_t = np.random.randn(n) + np.where(c == 0, mu_t_0, mu_t_1)
    y_v_noise = np.random.randn(n) + mu_v_noise
    y_t_noise = np.random.randn(n) + mu_t_noise
    x_v = m * y_v + (1 - m) * y_v_noise
    x_t = m * y_t_noise + (1 - m) * y_t
    # if we don't normalize the inputs the model will have hard time training
    x_v = x_v - x_v.mean()
    x_t = x_t - x_t.mean()
    plt.scatter(x_v, x_t, c=np.where(c == 0, 'blue', 'red'))
    plt.xlabel('visual modality')
    plt.ylabel('textual modality')

    plt.show()

    NUM_CLASSES = 2
    HIDDEN_STATE_DIM = 1  # using 1 as dimensionality makes it easy to plot z, as we'll do later on

    visual = tf.placeholder(tf.float32, shape=[None])
    textual = tf.placeholder(tf.float32, shape=[None])
    target = tf.placeholder(tf.int32, shape=[None])

    h_v = tf.layers.dense(tf.reshape(visual, [-1, 1]),
                          HIDDEN_STATE_DIM,
                          activation=tf.nn.tanh)
    h_t = tf.layers.dense(tf.reshape(textual, [-1, 1]),
                          HIDDEN_STATE_DIM,
                          activation=tf.nn.tanh)
    z = tf.layers.dense(tf.stack([visual, textual], axis=1),
                        HIDDEN_STATE_DIM,
                        activation=tf.nn.sigmoid)
    h = z * h_v + (1 - z) * h_t
    logits = tf.layers.dense(h, NUM_CLASSES)
    prob = tf.nn.sigmoid(logits)

    loss = tf.losses.sigmoid_cross_entropy(multi_class_labels=tf.one_hot(target, depth=2),
                                           logits=logits)
    optimizer = tf.train.AdamOptimizer(learning_rate=0.1)
    train_op = optimizer.minimize(loss)

    sess = tf.Session()


    def train(train_op, loss):
        sess.run(tf.global_variables_initializer())
        losses = []
        for epoch in range(100):
            _, l = sess.run([train_op, loss], {visual: x_v,
                                               textual: x_t,
                                               target: c})
            losses.append(l)

        plt.plot(losses, label='loss')
        plt.title('loss')
        plt.show()


    print("Training")
    train(train_op, loss)

    # create a mesh of points which will be used for inference
    resolution = 1000
    vs = np.linspace(x_v.min(), x_v.max(), resolution)
    ts = np.linspace(x_t.min(), x_t.max(), resolution)
    vs, ts = np.meshgrid(vs, ts)
    vs = np.ravel(vs)
    ts = np.ravel(ts)

    zs, probs = sess.run([z, prob], {visual: vs, textual: ts})


    def plot_evaluations(evaluation, cmap, title, labels):
        plt.scatter(((x_v - x_v.min()) * resolution / (x_v - x_v.min()).max()),
                    ((x_t - x_t.min()) * resolution / (x_t - x_t.min()).max()),
                    c=np.where(c == 0, 'blue', 'red'))
        plt.title(title, fontsize=14)
        plt.xlabel('visual modality')
        plt.ylabel('textual modality')
        plt.imshow(evaluation.reshape([resolution, resolution]),
                   origin='lower',
                   cmap=cmap,
                   alpha=0.5)
        cbar = plt.colorbar(ticks=[evaluation.min(), evaluation.max()])
        cbar.ax.set_yticklabels(labels)
        cbar.ax.tick_params(labelsize=13)


    plt.figure(figsize=(20, 7))
    plt.subplot(121)
    plot_evaluations(zs,
                     cmap='binary_r',
                     title='which modality the model attends',
                     labels=['$x_t$ is important', '$x_v$ is important'])
    plt.subplot(122)
    plot_evaluations(probs[:, 1],
                     cmap='bwr',
                     title='$C$ prediction',
                     labels=['$C=0$', '$C=1$'])

    plt.show()