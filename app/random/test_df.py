from Fancy_aggregations import integrals
import numpy as np

arr = np.array([60, 61, 61])

print(arr)

res = integrals.choquet_integral_symmetric(arr)
res2 = integrals.choquet_integral_CF(arr)
res3 = integrals.general_choquet_dx(arr, None)

print(res)
print(res2)
print(res3)

