import matplotlib.pyplot as plt
import tensorflow as tf
from sklearn.metrics import mean_squared_error
from sklearn.model_selection import train_test_split

def cnn(df):
    x, y = df, df
    print(x.shape)

    x = x.reshape(x.shape[0], x.shape[1], 1)
    print(x.shape)

    xtrain, xtest, ytrain, ytest = train_test_split(x, y, test_size=0.15)

    model = tf.keras.models.Sequential()
    model.add(tf.keras.layers.Conv1D(32, 2, activation="relu", input_shape=(13, 1)))
    model.add(tf.keras.layers.Flatten())
    model.add(tf.keras.layers.Dense(64, activation="relu"))
    model.add(tf.keras.layers.Dense(1))
    model.compile(loss="mse", optimizer="adam")
    model.summary()
    model.fit(xtrain, ytrain, batch_size=12, epochs=200, verbose=0)

    ypred = model.predict(xtest)
    print(model.evaluate(xtrain, ytrain))
    print("MSE: %.4f" % mean_squared_error(ytest, ypred))

    x_ax = range(len(ypred))
    plt.scatter(x_ax, ytest, s=5, color="blue", label="original")
    plt.plot(x_ax, ypred, lw=0.8, color="red", label="predicted")
    plt.legend()
    plt.show()
