from sklearn.linear_model import LogisticRegression
import matplotlib.pyplot as plt

def logistic_regression(df):
    df.fillna(df.mean(), inplace=True)
    X = df["TT_P1_Current_Travel_Time_Sec"].values.reshape(-1, 1)  # values converts it into a numpy array
    Y = df["TN_Noise_Level_DB"].values.reshape(-1, 1)  # -1 means that calculate the dimension of rows, but have 1 column

    log = LogisticRegression(C=1e5)
    log.fit(X, Y)
    Y_pred = log.predict(X)

    plt.scatter(X, Y)
    plt.plot(X, Y_pred, color='red')
    plt.show()
