import matplotlib.pyplot as plt
import numpy as np
import pandas as pd
from sklearn.metrics import mean_squared_error
from sklearn.preprocessing import LabelBinarizer
from sklearn.preprocessing import MinMaxScaler
from sklearn.preprocessing import StandardScaler
from tensorflow.keras import Input
from tensorflow.keras import Model
from tensorflow.keras.layers import Activation
from tensorflow.keras.layers import BatchNormalization
from tensorflow.keras.layers import Bidirectional
from tensorflow.keras.layers import Conv2D
from tensorflow.keras.layers import Dense
from tensorflow.keras.layers import Flatten
from tensorflow.keras.layers import LSTM
from tensorflow.keras.layers import Dropout
from tensorflow.keras.layers import MaxPooling2D
from tensorflow.keras.models import Sequential


def create_lstm(n_lag, n_features):
    model = Sequential()
    model.add(LSTM(8, return_sequences=True, input_shape=(n_lag, n_features)))
    model.add(LSTM(64, return_sequences=True))
    model.add(Dropout(0.3))
    model.add(LSTM(32, return_sequences=True))
    model.add(LSTM(4, return_sequences=False))
    model.add(Dense(1))

    return model


def preprocess(df, train, test):
    train['Hour_cos'] = [np.cos(x * (2 * np.pi / 24)) for x in train['Hour']]
    train['Hour_sin'] = [np.sin(x * (2 * np.pi / 24)) for x in train['Hour']]
    test['Hour_cos'] = [np.cos(x * (2 * np.pi / 24)) for x in test['Hour']]
    test['Hour_sin'] = [np.sin(x * (2 * np.pi / 24)) for x in test['Hour']]

    target = ['TN_Noise_Level_DB']

    continuous = ['IW_Temp_C', 'IW_Wind_Temp_C', 'IW_Rain_MMH', 'TT_P1_Speed_Diff_Kmh', 'TT_P1_Travel_Time_Diff_Sec',
                  'TT_P2_Speed_Diff_Kmh', 'TT_P2_Travel_Time_Diff_Sec', 'Hour_sin', 'Hour_cos']

    cs = StandardScaler()
    target_scaler = StandardScaler()
    train_target = target_scaler.fit_transform(train[target])
    test_target = target_scaler.transform(test[target])

    train_continuous = cs.fit_transform(train[continuous])
    test_continuous = cs.transform(test[continuous])

    categorical = ['TT_P1_Road_Type_Class']

    # one-hot encoding, all output features are now in the range [0, 1])
    weather_binarizer = LabelBinarizer().fit(df[categorical])
    train_categorical = weather_binarizer.transform(train[categorical])
    test_categorical = weather_binarizer.transform(test[categorical])

    categorical2 = ['IW_Weather_Cloudiness_Class']

    # one-hot encoding, all output features are now in the range [0, 1])
    weather_binarizer = LabelBinarizer().fit(df[categorical2])
    train_categorical2 = weather_binarizer.transform(train[categorical2])
    test_categorical2 = weather_binarizer.transform(test[categorical2])

    categorical3 = ['IW_Weather_Cloudiness_Class']

    # one-hot encoding, all output features are now in the range [0, 1])
    weather_binarizer = LabelBinarizer().fit(df[categorical3])
    train_categorical3 = weather_binarizer.transform(train[categorical3])
    test_categorical3 = weather_binarizer.transform(test[categorical3])

    categorical5 = ['Day_Of_Week']

    weather_binarizer = LabelBinarizer().fit(df[categorical5])
    train_categorical5 = weather_binarizer.transform(train[categorical5])
    test_categorical5 = weather_binarizer.transform(test[categorical5])

    # construct our training and testing data points by concatenating
    # the categorical features with the continuous features

    train_x = np.hstack(
        [train_target, train_continuous, train_categorical, train_categorical2, train_categorical3, train_categorical5])
    test_x = np.hstack(
        [test_target, test_continuous, test_categorical, test_categorical2, test_categorical3, test_categorical5])

    return train_x, test_x, target_scaler


def create_cnn(width, height, depth, filters=(16, 32, 64)):
    input_shape = (height, width, depth)
    channel_dim = -1

    inputs = Input(shape=input_shape)

    for (i, f) in enumerate(filters):
        if i == 0:
            x = inputs

        # CONV => RELU > BN => POOL
        x = Conv2D(f, (3, 3), padding="same")(x)
        x = Activation("relu")(x)
        x = BatchNormalization(axis=channel_dim)(x)
        x = MaxPooling2D(pool_size=(2, 2))(x)

    x = Flatten()(x)
    # x = Dense(16)(x)
    # x = Activation("relu")(x)
    # x = BatchNormalization(axis=channel_dim)(x)
    # # x = Dropout(0.3)(x)
    # #
    # # x = Dense(4)(x)
    # # x = Activation("relu")(x)

    model = Model(inputs, x)

    return model


def handle_mixed():
    df = pd.read_pickle('7-13feb-cameras.pkl')
    df = df.loc[df['TN_Noise_Level_DB'].notnull()]
    df = df.loc[df['Image'].notnull()]
    df = df.loc[df['Minute'] % 5 == 0]
    df = df.loc[df['TT_P1_Speed_Diff_Kmh'].notnull()]
    df = df.loc[df['TT_P1_Travel_Time_Diff_Sec'].notnull()]
    df = df.loc[df['TT_P2_Speed_Diff_Kmh'].notnull()]
    df = df.loc[df['TT_P2_Travel_Time_Diff_Sec'].notnull()]
    df = df.loc[df['IW_Temp_C'].notnull()]
    df = df.loc[df['IW_Wind_Temp_C'].notnull()]
    df = df.loc[df['IW_Rain_MMH'].notnull()]
    # df.sort_values('Datetime', inplace=True)

    images = df['Image'].to_numpy()
    images = np.array(list(images), dtype=np.float64)
    images = images / 255.

    n_outputs = 1
    n_lag = 24
    n_epoch = 100
    n_rows = df.shape[0]
    test_size = 0.1

    # CNN model to extract features from cameras
    cnn_model = create_cnn(256, 768, 3)

    print("-> Constructing train/test split")

    print("-> Preprocessing")

    image_features = cnn_model.predict(images, batch_size=4)

    df.drop('Image', axis=1)

    train_features = df[0:int(n_rows * (1 - test_size))]
    test_features = df[int(n_rows * (1 - test_size)):]
    train_image_features = image_features[0:int(n_rows * (1 - test_size))]
    test_image_features = image_features[int(n_rows * (1 - test_size)):]

    (train_preprocessed_features, test_preprocessed_features, scaler) = preprocess(df, train_features, test_features)

    train = np.concatenate([train_preprocessed_features], axis=1)
    test = np.concatenate([test_preprocessed_features], axis=1)

    # train = np.concatenate([train_preprocessed_features, train_image_features], axis=1)
    # test = np.concatenate([test_preprocessed_features, test_image_features], axis=1)

    n_features = train.shape[1]

    # Creating the final scaled frame
    data_s = pd.concat([pd.DataFrame(train), pd.DataFrame(test)])

    print(data_s.shape)

    x, y = timeseries_to_supervised(data_s.values, lag=n_lag, n_ahead=n_outputs, target_index=0)

    print(f'x: {x.shape}, y: {y.shape}')

    print(x[0])
    print(y[0])

    y_scaled_back = scaler.inverse_transform(y)

    print("Y - scaled back")
    print(y_scaled_back)

    train_x, train_y = x[0:int(x.shape[0] * (1 - test_size))], y[0:int(x.shape[0] * (1 - test_size))]
    test_x, test_y = x[int(x.shape[0] * (1 - test_size)):], y[int(x.shape[0] * (1 - test_size)):]

    print(f'Train: x: {train_x.shape}, y: {train_y.shape}')
    print(f'Test: x: {test_x.shape}, y: {test_y.shape}')

    model = create_lstm(n_lag, n_features)
    model.compile(optimizer='adam', loss='mape', metrics=['mape', 'mae', 'accuracy'])

    # train the model
    print("-> Training")
    history = model.fit(x=train_x, y=train_y,
                        validation_data=(test_x, test_y),
                        epochs=n_epoch, batch_size=36)

    print("-> Predicting noise levels")
    predictions = model.predict(test_x)
    print(f'Predictions shape: {predictions.shape}')

    predictions_y = np.array([p[0] for p in predictions]).reshape(-1, 1)

    print("Predictions:")
    print(predictions_y[0:3])

    naive = [p[len(p) - 1, 0].flatten() for p in test_x]
    moving_avg = [np.average(p[:, 0]) for p in test_x]

    scaled_predictions_y = scaler.inverse_transform(predictions_y)
    scaled_test_y = scaler.inverse_transform(test_y)
    scaled_moving_avg = scaler.inverse_transform(np.array(moving_avg).reshape(-1, 1))
    scaled_naive = scaler.inverse_transform(np.array(naive).reshape(-1, 1))

    print(f'RMSE - asdf: {np.sqrt(mean_squared_error(scaled_test_y, scaled_predictions_y))}')
    print(f'RMSE - naive: {np.sqrt(mean_squared_error(scaled_test_y, scaled_naive))}')
    print(f'RMSE - moving av: {np.sqrt(mean_squared_error(scaled_test_y, scaled_moving_avg))}')

    # ON TRAINING SET
    print("-> Predicting noise levels")
    train_predictions = model.predict(train_x)
    train_predictions_y = np.array([p[0] for p in train_predictions]).reshape(-1, 1)

    train_naive = [p[len(p) - 1, 0].flatten() for p in train_x]
    train_moving_avg = [np.average(p[:, 0]) for p in train_x]

    train_scaled_predictions_y = scaler.inverse_transform(train_predictions_y)
    train_scaled_test_y = scaler.inverse_transform(train_y)
    train_scaled_moving_avg = scaler.inverse_transform(np.array(train_moving_avg).reshape(-1, 1))
    train_scaled_naive = scaler.inverse_transform(np.array(train_naive).reshape(-1, 1))

    # TRAINING SET END

    print("True_scaled:")
    print(scaled_test_y[0:3])
    print("Predictions_scaled:")
    print(scaled_predictions_y[0:3])
    # print((scaled_test_y - scaled_predictions_y)[0:3])

    diff = predictions.flatten() - test_y
    percent_diff = (diff / test_y) * 100
    abs_percent_diff = np.abs(percent_diff)
    # compute the mean and standard deviation of the absolute percentage
    # difference
    mean = np.mean(abs_percent_diff)
    std = np.std(abs_percent_diff)

    print("-> avg. noise level: {}, std noise level: {}".format(
        df["TN_Noise_Level_DB"].mean(),
        df["TN_Noise_Level_DB"].std()))
    print("-> mean: {:.2f}%, std: {:.2f}%".format(mean, std))

    plt.plot(np.concatenate([train_scaled_test_y, scaled_test_y]), label='Noise')
    # plt.plot(np.concatenate([train_scaled_naive, scaled_naive]), label='Naive')
    plt.plot(np.concatenate([train_scaled_predictions_y, scaled_predictions_y]), label='Model')
    plt.plot(np.concatenate([train_scaled_moving_avg, scaled_moving_avg]), label='Moving avg')

    # plt.plot(np.concatenate([train_y, test_y]), label='Noise')
    # # plt.plot(np.concatenate([train_scaled_naive, scaled_naive]), label='Naive')
    # plt.plot(np.concatenate([predictions_y, train_predictions_y]), label='Model')
    # plt.plot(np.concatenate([moving_avg, train_moving_avg]), label='Moving avg')


    plt.legend()
    plt.show()


def timeseries_to_supervised(ts: np.array, lag=1, n_ahead=1, target_index=0) -> tuple:
    """
    A method to create X and Y matrix from a time series array
    """
    # Extracting the number of features that are passed from the array
    n_features = ts.shape[1]

    # Creating placeholder lists
    x, y = [], []

    if len(ts) - lag <= 0:
        x.append(ts)
    else:
        for i in range(len(ts) - lag - n_ahead):
            y.append(ts[(i + lag):(i + lag + n_ahead), target_index])
            x.append(ts[i:(i + lag)])

    x, y = np.array(x), np.array(y)

    # Reshaping the X array to an RNN input shape
    x = np.reshape(x, (x.shape[0], lag, n_features))

    return x, y
