from sklearn.svm import SVR
import numpy as np
import pandas as pd

x_pred = np.array([(60, 0), (61, 0), (62, 0)])
img_x_pred = np.array([(50, 0), (55, 0), (55, 0)])
x_true = np.array([60, 61, 62])

merged_train_data = np.zeros(shape=(len(x_pred), 2))
merged_train_data[:, 1] = x_pred[:, 0]
merged_train_data[:, 0] = img_x_pred[:, 0]

y_pred = np.array([(60, 0), (61, 0), (62, 0)])
img_pred = np.array([(50, 0), (55, 0), (55, 0)])
y_true = [63, 62, 62]

merged_data = np.zeros(shape=(len(y_pred), 2))

merged_data[:, 0] = img_pred[:, 0]
merged_data[:, 1] = y_pred[:, 0]


regressor = SVR(kernel='rbf')
regressor.fit(merged_train_data[:, :], x_true)

df_svm_pred = regressor.predict(merged_data[:, :].astype(np.float32))
print(df_svm_pred)
