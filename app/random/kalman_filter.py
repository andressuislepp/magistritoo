import numpy as np
from filterpy.common import Q_discrete_white_noise
from filterpy.kalman import KalmanFilter

f = KalmanFilter(dim_x=2, dim_z=2)

f.x = np.array([2., 2.])  # inputs

# transition matrix
f.F = np.array([[1., 1.],
                [1., 1.]])

# measurement function
f.H = np.array([[1., 0.]])

# covariance matrix, use np.eye
f.P = np.array([[1000., 0.],
                [0., 1000.]])

# measurement noise
f.R = 5

# process noise
f.Q = Q_discrete_white_noise(dim=2, dt=0.1, var=0.13)

f.predict()
print(f.x)
f.update(np.array([3., 3.5]).reshape(-1, 1))
print(f.x)
# f.update([3.])
# print(f.x)


