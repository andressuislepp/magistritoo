from sklearn.linear_model import LinearRegression
import matplotlib.pyplot as plt

def linear_regression(df):
    # df.fillna(df.mean(), inplace=True)
    X = df["TT_P1_Travel_Time_Diff_Sec"].values.reshape(-1, 1)  # values converts it into a numpy array
    Y = df["TN_Noise_Level_DB"].values.reshape(-1, 1)  # -1 means that calculate the dimension of rows, but have 1 column

    linear_regressor = LinearRegression()
    linear_regressor.fit(X, Y)
    Y_pred = linear_regressor.predict(X)

    plt.scatter(X, Y)
    plt.plot(X, Y_pred, color='red')
    plt.show()