import argparse
import sys

import pandas as pd
import tensorflow as tf

import data_importer
import image_importer
from sklearn.preprocessing import MinMaxScaler

from models import lstm_univariate, lstm_df_full, lstm_img_cnn, lstm_nofusion, lstm_fusion_kf_2vec, lstm_fusion_ukf_2vec, lstm_fusion_skf_2vec
from models.discarded import lstm_vgg16_ukf, lstm_img_vgg16, lstm_img_vgg16_md, lstm_vgg16, lstm_vgg16_ukf_md, \
    lstm_fusion_ukf, lstm_fusion_kf, lstm_fusion_skf, lstm_cnn
from models.timeseries import moving_average, naive, arima, linear_regression

parser = argparse.ArgumentParser(description='ASDF - Noise prediction')

parser.add_argument('--n_epochs', type=int, help='Epochs for training')
parser.add_argument('--n_lag', type=int, help='Lag/look back for LSTM')
parser.add_argument('--n_outputs', type=int, help='Time steps to predict for LSTM')
parser.add_argument('--test_size', type=float, help='Test size for LSTM')
parser.add_argument('--method', help='Method',
                    choices=['process', 'load_data', 'load_images', 'arima', 'linear_regression', 'lstm_img_cnn', 'lstm_df_full', 'lstm_univariate', 'lstm_nofusion', 'lstm_fusion_kf', 'lstm_img_vgg16', 'lstm_img_vgg16_md', 'lstm_cnn_ukf', 'lstm_vgg16_ukf', 'lstm_vgg16_ukf_md', 'lstm_fusion_kf_2vec', 'lstm_fusion_ukf', 'lstm_fusion_ukf_2vec', 'lstm_fusion_skf', 'lstm_fusion_skf_2vec', 'lstm_cnn', 'naive', 'moving_average', 'lstm_vgg16'])


tf.keras.utils.set_random_seed(
    200
)

def load_for_import():
    df = pd.read_pickle('fe10mar18.pkl')

    return df

def load():
    df, target_scaler = load_with_images('sm')
    df.drop('Image', axis=1)

    df.info()

    return df, target_scaler


def load_with_images(size):
    if size == 'md':
        df = pd.read_pickle('10feb-06mar-cameras-md.pkl')
        df.drop('Image_x', axis=1)
        df = df.rename(columns={'Image_y': 'Image'})
    else:
        df = pd.read_pickle('10feb-06mar-cameras-sm.pkl')
    df.sort_values('Datetime', inplace=True)

    # df['TN_Noise_Level_DB'] = df['TN_Noise_Level_DB'].fillna(method='bfill')

    df = df.fillna(method='bfill', limit=6)
    df = df.loc[df['Minute'] % 5 == 0]
    df = df.fillna(method='bfill', limit=2)

    df.info()

    # df = df.head(250)

    df = df.loc[df['TN_Noise_Level_DB'].notnull()]
    df = df.loc[df['TT_P1_Speed_Diff_Kmh'].notnull()]
    df = df.loc[df['TT_P1_Travel_Time_Diff_Sec'].notnull()]
    df = df.loc[df['TT_P2_Speed_Diff_Kmh'].notnull()]
    df = df.loc[df['TT_P2_Travel_Time_Diff_Sec'].notnull()]
    df = df.loc[df['IW_Temp_C'].notnull()]
    df = df.loc[df['IW_Wind_Temp_C'].notnull()]
    df = df.loc[df['IW_Rain_MMH'].notnull()]
    df = df.loc[df['Image'].notnull()]
    df = df.loc[df['IW_Wind_Speed_MH'].notnull()]
    df.sort_values('Datetime', inplace=True)

    target = ['TN_Noise_Level_DB']
    target_scaler = MinMaxScaler(feature_range=(0, 1))
    df['TN_Noise_Level_DB'] = target_scaler.fit_transform(df[target])

    return df, target_scaler


def import_data_to_pickle():
    tallinn_noise, tallinn_weather, ilmee_weather, tomtom_traffic_p1, tomtom_traffic_p2 = data_importer.import_all()

    df = pd.merge(tallinn_weather, tallinn_noise,
                  how='left',
                  on=['Date', 'Hour', 'Minute'])

    df = pd.merge(df, ilmee_weather,
                  how='left',
                  on=['Date', 'Hour', 'Minute'])

    df = pd.merge(df, tomtom_traffic_p1,
                  how='left',
                  on=['Date', 'Hour', 'Minute'])

    df = pd.merge(df, tomtom_traffic_p2,
                  how='left',
                  on=['Date', 'Hour', 'Minute'])

    df.to_pickle('fe10mar18.pkl')
    return df


def load_image_data(df):
    images = image_importer.import_all()

    df = pd.merge(df, images,
                  how='left',
                  on=['Date', 'Hour', 'Minute'])

    df = df.loc[df['Image'].notnull()]
    df.to_pickle('10feb-06mar-cameras-sm.pkl')

    df.info()


if __name__ == '__main__':

    print("<- startup")
    print("<- dataset: 10 Feb - 6 March")

    args = parser.parse_args()

    print(f'<- method {args.method}')
    print(f'<- n_epochs {args.n_epochs}')
    print(f'<- n_lag {args.n_lag}')
    print(f'<- n_outputs {args.n_outputs}')
    print(f'<- test_size {args.test_size}')

    if args.method == 'load_data':
        # --- LOAD NUMERIC/CATEGORICAL DATA TO PICKLE
        print("<- loading data to pickle")
        df = import_data_to_pickle()
        print("<- loading data to pickle finished")
        df.info()
        sys.exit(-1)

    if args.method == 'load_images':
        # LOADING
        # --- LOAD IMAGE DATA FROM PICKLE TO PICKLE
        df = load_for_import()
        load_image_data(df)
        print("<- loading data to pickle finished")
        sys.exit(-1)

    if args.method == 'lstm_univariate':
        df, target_scaler = load()
        df.info()
        lstm_univariate.handle_lstm(target_scaler, df, args.n_lag, args.n_epochs, args.n_outputs, args.test_size)

    if args.method == 'lstm_nofusion':
        df, target_scaler = load()
        df.info()

        lstm_nofusion.handle_lstm_nofusion(target_scaler, df, args.n_lag, args.n_epochs, args.n_outputs, args.test_size)
        sys.exit(-1)

    if args.method == 'lstm_fusion_kf':
        df, target_scaler = load()
        df.info()

        lstm_fusion_kf.handle_lstm_kf(target_scaler, df, args.n_lag, args.n_epochs, args.n_outputs, args.test_size)
        sys.exit(-1)


    if args.method == 'lstm_fusion_kf_2vec':
        df, target_scaler = load()
        df.info()

        lstm_fusion_kf_2vec.handle_lstm_kf_2vec(target_scaler, df, args.n_lag, args.n_epochs, args.n_outputs, args.test_size)
        sys.exit(-1)

    if args.method == 'lstm_fusion_ukf':
        df, target_scaler = load()
        df.info()

        lstm_fusion_ukf.handle_lstm_ukf(target_scaler, df, args.n_lag, args.n_epochs, args.n_outputs, args.test_size)
        sys.exit(-1)

    if args.method == 'lstm_fusion_ukf_2vec':
        df, target_scaler = load()
        df.info()

        lstm_fusion_ukf_2vec.handle_lstm_ukf_2vec(target_scaler, df, args.n_lag, args.n_epochs, args.n_outputs, args.test_size)
        sys.exit(-1)

    if args.method == 'lstm_fusion_skf':
        df, target_scaler = load()
        df.info()

        lstm_fusion_skf.handle_lstm_skf(target_scaler, df, args.n_lag, args.n_epochs, args.n_outputs, args.test_size)
        sys.exit(-1)

    if args.method == 'lstm_fusion_skf_2vec':
        df, target_scaler = load()
        df.info()

        lstm_fusion_skf_2vec.handle_lstm_skf_2vec(target_scaler, df, args.n_lag, args.n_epochs, args.n_outputs, args.test_size)
        sys.exit(-1)

    if args.method == 'lstm_df_full':
        df, target_scaler = load_with_images('sm')
        df.info()
        lstm_df_full.handle_df_full(target_scaler, df, args.n_lag, args.n_epochs, args.n_outputs, args.test_size)

    if args.method == 'lstm_cnn':
        df, target_scaler = load_with_images('sm')
        df.info()
        lstm_cnn.handle_lstm_cnn(df, args.n_lag, args.n_epochs, args.n_outputs, args.test_size)

    # if args.method == 'lstm_cnn_ukf':
    #     df, target_scaler = load_with_images('sm')
    #     df.info()
    #     lstm_cnn_ukf.handle_lstm_cnn_ukf(target_scaler, df, args.n_lag, args.n_epochs, args.n_outputs, args.test_size)

    if args.method == 'lstm_img_vgg16':
        df, target_scaler = load_with_images('sm')
        df.info()
        lstm_img_vgg16.handle_lstm_img_vgg16(target_scaler, df, args.n_lag, args.n_epochs, args.n_outputs, args.test_size)

    if args.method == 'lstm_img_cnn':
        df, target_scaler = load_with_images('sm')
        df.info()
        lstm_img_cnn.handle_lstm_img_cnn(target_scaler, df, args.n_lag, args.n_epochs, args.n_outputs,
                                             args.test_size)

    if args.method == 'lstm_img_vgg16_md':
        df, target_scaler = load_with_images('md')
        df.info()
        lstm_img_vgg16_md.handle_lstm_img_vgg16_md(df, args.n_lag, args.n_epochs, args.n_outputs, args.test_size)

    if args.method == 'lstm_vgg16':
        df, target_scaler = load_with_images('sm')
        df.info()
        lstm_vgg16.handle_lstm_cnn(df, args.n_lag, args.n_epochs, args.n_outputs, args.test_size)

    if args.method == 'lstm_vgg16_ukf':
        df, target_scaler = load_with_images('sm')
        df.info()
        lstm_vgg16_ukf.handle_lstm_vgg16_ukf(target_scaler, df, args.n_lag, args.n_epochs, args.n_outputs, args.test_size)

    if args.method == 'lstm_vgg16_ukf_md':
        df, target_scaler = load_with_images('md')
        df.info()
        lstm_vgg16_ukf_md.handle_lstm_vgg16_ukf_md(df, args.n_lag, args.n_epochs, args.n_outputs, args.test_size)

    if args.method == 'moving_average':
        df, target_scaler = load()
        df.info()
        moving_average.handle_moving_average(target_scaler, df, args.n_lag, args.n_outputs, args.test_size)

    if args.method == 'naive':
        df, target_scaler = load()
        df.info()
        naive.handle_naive(target_scaler, df, args.n_lag, args.n_outputs, args.test_size)

    if args.method == 'arima':
        df, target_scaler = load()
        df.info()
        arima.handle_arima(target_scaler, df, args.n_lag, args.n_outputs, args.test_size)

    if args.method == 'linear_regression':
        df, target_scaler = load()
        df.info()
        linear_regression.handle_linear_regression(target_scaler, df, args.n_lag, args.n_outputs, args.test_size)
