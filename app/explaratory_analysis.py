import argparse

import cv2
import matplotlib.pyplot as plt
import pandas as pd
import seaborn as sns
import tensorflow as tf
from matplotlib import rcParams
from sklearn.preprocessing import MinMaxScaler

import data_importer
import image_importer

rcParams.update({'figure.autolayout': True})
parser = argparse.ArgumentParser(description='ASDF - Noise prediction')

parser.add_argument('--n_epochs', type=int, help='Epochs for training')
parser.add_argument('--n_lag', type=int, help='Lag/look back for LSTM')
parser.add_argument('--n_outputs', type=int, help='Time steps to predict for LSTM')
parser.add_argument('--test_size', type=float, help='Test size for LSTM')
parser.add_argument('--method', help='Method',
                    choices=['process', 'load_data', 'load_images', 'lstm_img_cnn', 'lstm_df_full', 'lstm_univariate',
                             'lstm_nofusion', 'lstm_fusion_kf', 'lstm_img_vgg16', 'lstm_img_vgg16_md', 'lstm_cnn_ukf',
                             'lstm_vgg16_ukf', 'lstm_vgg16_ukf_md', 'lstm_fusion_kf_2vec', 'lstm_fusion_ukf',
                             'lstm_fusion_ukf_2vec', 'lstm_fusion_skf', 'lstm_fusion_skf_2vec', 'lstm_cnn', 'naive',
                             'moving_average', 'lstm_vgg16'])

tf.keras.utils.set_random_seed(
    200
)


def load():
    df, target_scaler = load_with_images('sm')
    df.drop('Image', axis=1)

    df.info()

    return df, target_scaler


def load_with_images(size):
    if size == 'md':
        df = pd.read_pickle('10feb-06mar-cameras-md.pkl')
        df.drop('Image_x', axis=1)
        df = df.rename(columns={'Image_y': 'Image'})
    else:
        df = pd.read_pickle('10feb-06mar-cameras-sm.pkl')
    df.sort_values('Datetime', inplace=True)

    # df['TN_Noise_Level_DB'] = df['TN_Noise_Level_DB'].fillna(method='bfill')

    df = df.fillna(method='bfill', limit=12)
    df = df.loc[df['Minute'] % 5 == 0]
    df = df.fillna(method='bfill', limit=2)

    df.info()
    # df = df.head(250)

    df = df.loc[df['TN_Noise_Level_DB'].notnull()]
    df = df.loc[df['TT_P1_Speed_Diff_Kmh'].notnull()]
    df = df.loc[df['TT_P1_Travel_Time_Diff_Sec'].notnull()]
    df = df.loc[df['TT_P2_Speed_Diff_Kmh'].notnull()]
    df = df.loc[df['TT_P2_Travel_Time_Diff_Sec'].notnull()]
    df = df.loc[df['IW_Temp_C'].notnull()]
    df = df.loc[df['IW_Wind_Temp_C'].notnull()]
    df = df.loc[df['IW_Rain_MMH'].notnull()]
    df = df.loc[df['Image'].notnull()]
    df = df.loc[df['IW_Wind_Speed_MH'].notnull()]
    df.sort_values('Datetime', inplace=True)

    target = ['TN_Noise_Level_DB']
    target_scaler = MinMaxScaler(feature_range=(0, 1))
    df['TN_Noise_Level_DB'] = target_scaler.fit_transform(df[target])

    return df, target_scaler


def import_data_to_pickle():
    tallinn_noise, tallinn_weather, ilmee_weather, tomtom_traffic_p1, tomtom_traffic_p2 = data_importer.import_all()

    df = pd.merge(tallinn_weather, tallinn_noise,
                  how='left',
                  on=['Date', 'Hour', 'Minute', 'Day_Of_Month', 'Day_Of_Week'])

    df = pd.merge(df, ilmee_weather,
                  how='left',
                  on=['Date', 'Hour', 'Minute', 'Day_Of_Month', 'Day_Of_Week'])

    df = pd.merge(df, tomtom_traffic_p1,
                  how='left',
                  on=['Date', 'Hour', 'Minute', 'Day_Of_Month', 'Day_Of_Week'])

    df = pd.merge(df, tomtom_traffic_p2,
                  how='left',
                  on=['Date', 'Hour', 'Minute', 'Day_Of_Month', 'Day_Of_Week'])

    df.to_pickle('fe10mar18.pkl')
    return df


def load_image_data(df):
    images = image_importer.import_all()

    df = pd.merge(df, images,
                  how='left',
                  on=['Date', 'Hour', 'Minute'])

    df.to_pickle('10feb-06mar-cameras-md.pkl')

    df.info()


if __name__ == '__main__':
    print("<- startup")
    print("<- dataset: 10 Feb - 6 March")

    df = pd.read_pickle('10feb-06mar-cameras-sm.pkl')
    df.sort_values('Datetime', inplace=True)
    df.info()

    # Missing values
    print(f'--> Target variable analysis')
    df_missing = df.head(2000)
    plt.figure(1)
    fig, ax = plt.subplots()
    ax.plot_date(df_missing['Datetime'], df_missing['TN_Noise_Level_DB'], marker='', linestyle='-', c='#44CF6C')
    ax.legend(['Noise'], loc='upper right')
    plt.ylabel('[dB]')
    fig.autofmt_xdate()
    plt.savefig('noise-timeseries.eps', format='eps')
    plt.show()

    print(f'Noise value is present for {1925 / 7252 * 100}%')
    print(df[['TN_Noise_Level_DB']].describe())

    plt.figure(3)
    plt.ylabel('Probability')
    plt.xlabel('Noise [dB]')
    plt.hist(df[['TN_Noise_Level_DB']], density=True, bins=30, rwidth=0.8, color='#44CF6C')

    plt.savefig('noise-histogram.eps', format='eps')
    plt.show()

    print(f'--> Cameras example')
    cv2.imwrite('cameras.png', df.loc[4570]['Image'])

    print(f'--> Variable selection: continious values')
    df_corr = df[['TN_Noise_Level_DB', 'Minute_Of_Day', 'TT_P1_Speed_Diff_Kmh', 'TT_P2_Speed_Diff_Kmh',
                  'TT_P1_Travel_Time_Diff_Sec', 'TT_P2_Travel_Time_Diff_Sec', 'WT_Road_Temperature_C',
                  'WT_Air_Humidity_Percent', 'IW_Temp_C', 'IW_Wind_Temp_C', 'IW_Wind_Speed_MH', 'IW_Rain_MMH']]
    plt.figure(2)
    corr = df_corr.corr()
    columns = ['Noise', 'Min of day', 'P1 speed diff', 'P2 speed diff', 'P1 time diff', 'P2 time diff', 'Road temperature', 'Humidity', 'Temperature', 'Wind temperature', 'Wind speed', 'Rainfall']
    sns.set(font_scale=0.75)
    hm = sns.heatmap(corr,
                     cbar=True,
                     annot=True,
                     square=True,
                     fmt='.2f',
                     annot_kws={'size': 8},
                     yticklabels=corr.columns,
                     xticklabels=corr.columns,
                     cmap="Spectral_r")


    plt.savefig('correlation.eps', format='eps')
    plt.show()

    # Rain has close to zero correlation -> let's investigate
    print(df[['IW_Rain_MMH']].describe())
    plt.figure(5)
    plt.ylabel('Probability')
    plt.xlabel('Rain [mm/h]')
    plt.hist(df[['IW_Rain_MMH']], density=True, bins=30, rwidth=0.8, color='#44CF6C')

    plt.savefig('rain-histogram.eps', format='eps')
    plt.show()

    print(f'--> Variable selection: categorical values: TomTom')
    print(df['TT_P1_Is_Road_Closed'].value_counts())
    print(df['TT_P2_Is_Road_Closed'].value_counts())
    print(df['TT_P1_Road_Type_Class'].value_counts())
    print(df['TT_P2_Road_Type_Class'].value_counts())

    print(f'--> Variable selection: categorical values: Weather')
    print(df['IW_Wind_Direction_Mark'].value_counts())
    print(df['IW_Weather_Cloudiness_Class'].value_counts())
    print(df['IW_Weather_Coldness_Class'].value_counts())
