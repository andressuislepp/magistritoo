import json
from datetime import datetime

import pandas as pd
from bs4 import BeautifulSoup


def compute_datetime_features(df, drop_ts=True):
    df['Datetime'] = df.apply(lambda row: datetime.fromisoformat(row['Timestamp']), axis=1)
    df['Date'] = df.apply(lambda row: row['Datetime'].strftime("%Y-%m-%d"), axis=1)
    df['Hour'] = df.apply(lambda row: row['Datetime'].hour, axis=1)
    df['Minute'] = df.apply(lambda row: row['Datetime'].minute, axis=1)
    df['Minute_Of_Day'] = df.apply(lambda row: (row['Hour'] * 60) + row['Minute'], axis=1)
    df['Day_Of_Month'] = df.apply(lambda row: row['Datetime'].day, axis=1)
    df['Day_Of_Week'] = df.apply(lambda row: row['Datetime'].weekday(), axis=1)
    df['Is_Weekend'] = df.apply(lambda row: row['Day_Of_Week'] == 5 or row['Day_Of_Week'] == 6, axis=1)

    # Otherwise duplicates are created when merging dataframes
    if drop_ts:
        df = df.drop("Timestamp", axis=1)
        df = df.drop("Datetime", axis=1)
        df = df.drop("Is_Weekend", axis=1)
        df = df.drop("Day_Of_Month", axis=1)
        df = df.drop("Day_Of_Week", axis=1)
        df = df.drop("Minute_Of_Day", axis=1)

    return df


def import_tallinn_noise():
    df = pd.read_csv('data/fe10mar18/tallinn-noise-data.csv',
                     skiprows=1,
                     names=["Timestamp", "TN_Noise_Level_DB"])
    df = compute_datetime_features(df)

    return df


def import_tallinn_traffic():
    df = pd.read_csv('data/fe10mar18/tallinn-traffic-data.csv',
                     skiprows=1,
                     names=["Timestamp", "TN_Vehicle_Count"])
    df = compute_datetime_features(df)

    return df


def compose_safe_fn(fn):
    def safe_fn(row):
        try:
            return fn(row)
        except:
            return None

    return safe_fn


def import_ilmee_weather():
    df = pd.read_csv('data/fe10mar18/ilmee-weather.csv', sep="|",
                     names=['Timestamp', 'Data'])

    df['Json'] = df.apply(lambda row: json.loads(row['Data']), axis=1)
    df['IW_Temp_C'] = df.apply(lambda row: float(row['Json']['ilm']['temp'].replace('&deg;C', '')), axis=1)
    df['IW_Wind_Temp_C'] = df.apply(lambda row: float(row['Json']['ilm']['windchill'].replace('&deg;C', '')), axis=1)
    # df['IW_Wind_Direction_Deg'] = df.apply(lambda row: float(row['Json']['ilm']['d_suund'].replace('&deg;', '')),
    #                                        axis=1)
    df['IW_Wind_Direction_Mark'] = df.apply(lambda row: row['Json']['ilm']['f_suund'], axis=1)
    df['IW_Weather_Cloudiness_Class'] = df.apply(
        lambda row: row['Json']['ilm']['pilv'].replace('url(/tingmargid/2015/PrognoosiPilv.php3?P=n_4', '')
            .replace('url(/tingmargid/2015/PrognoosiPilv.php3?P=2', '')
            .replace('url(/tingmargid/2015/PrognoosiPilv.php3?P=n_2', '')
            .replace('url(/tingmargid/2015/PrognoosiPilv.php3?P=n_0', '')
            .replace('url(/tingmargid/2015/PrognoosiPilv.php3?P=0', '')
            .replace('url(/tingmargid/2015/PrognoosiPilv.php3?P=4', '').replace(')', ''), axis=1)
    df['IW_Weather_Coldness_Class'] = df.apply(lambda row: row['Json']['ilm']['sk'], axis=1)
    df['IW_Air_Pressure_mmHg'] = df.apply(lambda row: float(row['Json']['ilm']['rohk'].replace(' mmHg', '')), axis=1)
    df['IW_Air_Humidity_Percent'] = df.apply(
        compose_safe_fn(lambda row: float(row['Json']['ilm']['niiskus'].replace(' %', ''))), axis=1)
    df['IW_Wind_Speed_MH'] = df.apply(
        compose_safe_fn(lambda row: float(row['Json']['ilm']['tuul'].replace(' m/s', ''))),
        axis=1)
    df['IW_Rain_MMH'] = df.apply(compose_safe_fn(lambda row: float(row['Json']['ilm']['vihm'].replace(' mm/h', ''))),
                                 axis=1)
    df['IW_Sunrise_Hour'] = df.apply(lambda row: row['Json']['sun']['up'].split(':')[0], axis=1)
    df['IW_Sunrise_Minute'] = df.apply(lambda row: row['Json']['sun']['up'].split(':')[1], axis=1)
    df['IW_Sunset_Hour'] = df.apply(lambda row: row['Json']['sun']['down'].split(':')[0], axis=1)
    df['IW_Sunset_Minute'] = df.apply(lambda row: row['Json']['sun']['down'].split(':')[1], axis=1)

    df = df.drop("Data", axis=1)
    df = df.drop("Json", axis=1)
    df = compute_datetime_features(df, drop_ts=False)

    return df


def import_tallinn_weather():
    df = pd.read_csv('data/fe10mar18/tallinn-weather.csv', sep="½",
                     names=['Timestamp', 'Data'])

    df['Html'] = df.apply(compose_safe_fn(lambda row: BeautifulSoup(row['Data'], 'html.parser').find_all("ul", "uk-list")[9]), axis=1)
    df['WT_Temperature_C'] = df.apply(compose_safe_fn(
        lambda row: float(row['Html'].find_all("li")[1].contents[0].replace('ÕHK: ', '').replace(' °C ', ''))), axis=1)
    df['WT_Air_Humidity_Percent'] = df.apply(compose_safe_fn(
        lambda row: float(row['Html'].find_all("li")[2].contents[0].replace('ÕHUNIISKUS: ', '').replace(' % ', ''))),
        axis=1)
    df['WT_Road_Temperature_C'] = df.apply(compose_safe_fn(
        lambda row: float(row['Html'].find_all("li")[4].contents[0].replace('TEE: ', '').replace('°C ', ''))), axis=1)

    df = df.drop("Data", axis=1)
    df = df.drop("Html", axis=1)
    df = compute_datetime_features(df)

    return df


def import_tomtom_traffic_p1():
    df = pd.read_csv('data/fe10mar18/tomtom-traffic.csv', sep="|",
                     names=['Measure_Point', 'Timestamp', 'Data'])

    df = df.loc[df['Measure_Point'] == "p1"]


    df['Json'] = df.apply(compose_safe_fn(lambda row: json.loads(row['Data'])['flowSegmentData']), axis=1)
    df['TT_P1_Road_Type_Class'] = df.apply(compose_safe_fn(lambda row: row['Json']['frc']), axis=1)
    df['TT_P1_Current_Speed_Kmh'] = df.apply(compose_safe_fn(lambda row: row['Json']['currentSpeed']), axis=1)
    df['TT_P1_Freeflow_Speed_Kmh'] = df.apply(compose_safe_fn(lambda row: row['Json']['freeFlowSpeed']), axis=1)
    df['TT_P1_Speed_Diff_Kmh'] = df.apply(
        compose_safe_fn(lambda row: row['TT_P1_Freeflow_Speed_Kmh'] - row['TT_P1_Current_Speed_Kmh']),
        axis=1)
    df['TT_P1_Current_Travel_Time_Sec'] = df.apply(compose_safe_fn(lambda row: row['Json']['currentTravelTime']),
                                                   axis=1)
    df['TT_P1_Freeflow_Travel_Time_Sec'] = df.apply(compose_safe_fn(lambda row: row['Json']['freeFlowTravelTime']),
                                                    axis=1)
    df['TT_P1_Travel_Time_Diff_Sec'] = df.apply(
        compose_safe_fn(lambda row: row['TT_P1_Current_Travel_Time_Sec'] - row['TT_P1_Freeflow_Travel_Time_Sec']),
        axis=1)
    df['TT_P1_Is_Road_Closed'] = df.apply(compose_safe_fn(lambda row: row['Json']['roadClosure']), axis=1)

    df = df.drop("Data", axis=1)
    df = df.drop("Json", axis=1)
    df = df.drop("Measure_Point", axis=1)
    df = compute_datetime_features(df)

    return df


def import_tomtom_traffic_p2():
    df = pd.read_csv('data/fe10mar18/tomtom-traffic.csv', sep="|",
                     names=['Measure_Point', 'Timestamp', 'Data'])
    df = df.loc[df['Measure_Point'] == "p2"]

    df['Json'] = df.apply(compose_safe_fn(lambda row: json.loads(row['Data'])['flowSegmentData']), axis=1)
    df['TT_P2_Road_Type_Class'] = df.apply(compose_safe_fn(lambda row: row['Json']['frc']), axis=1)
    df['TT_P2_Current_Speed_Kmh'] = df.apply(compose_safe_fn(lambda row: row['Json']['currentSpeed']), axis=1)
    df['TT_P2_Freeflow_Speed_Kmh'] = df.apply(compose_safe_fn(lambda row: row['Json']['freeFlowSpeed']), axis=1)
    df['TT_P2_Speed_Diff_Kmh'] = df.apply(
        compose_safe_fn(lambda row: row['TT_P2_Freeflow_Speed_Kmh'] - row['TT_P2_Current_Speed_Kmh']), axis=1)
    df['TT_P2_Current_Travel_Time_Sec'] = df.apply(compose_safe_fn(lambda row: row['Json']['currentTravelTime']),
                                                   axis=1)
    df['TT_P2_Freeflow_Travel_Time_Sec'] = df.apply(compose_safe_fn(lambda row: row['Json']['freeFlowTravelTime']),
                                                    axis=1)
    df['TT_P2_Travel_Time_Diff_Sec'] = df.apply(
        compose_safe_fn(lambda row: row['TT_P2_Current_Travel_Time_Sec'] - row['TT_P2_Freeflow_Travel_Time_Sec']),
        axis=1)

    df['TT_P2_Is_Road_Closed'] = df.apply(compose_safe_fn(lambda row: row['Json']['roadClosure']), axis=1)

    df = df.drop("Data", axis=1)
    df = df.drop("Json", axis=1)
    df = df.drop("Measure_Point", axis=1)
    df = compute_datetime_features(df)

    return df


def import_all():
    tallinn_noise = import_tallinn_noise()
    # tallinn_traffic = import_tallinn_traffic()
    ilmee_weather = import_ilmee_weather()
    tomtom_traffic_p1 = import_tomtom_traffic_p1()
    tomtom_traffic_p2 = import_tomtom_traffic_p2()
    tallinn_weather = import_tallinn_weather()

    return tallinn_noise, tallinn_weather, ilmee_weather, tomtom_traffic_p1, tomtom_traffic_p2
