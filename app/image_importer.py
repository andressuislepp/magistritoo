import sys

import cv2
import datetime
from os import listdir
from itertools import groupby
import numpy as np
import pandas as pd

def parse_filename(path, filename):
    full_path = f'{path}/{filename}'
    camera_name = filename.split('__')[0]
    date_epoch = int(filename.split('__')[1].split('.jpg')[0])
    date = datetime.datetime.fromtimestamp((date_epoch + (2 * 24 * 60 * 60))/1000)

    return full_path, filename, camera_name, date_epoch, date.strftime("%Y-%m-%d"), date.hour, date.minute

def process_images(images):
    images = sorted(images, key=lambda i: (i[4], i[5], i[6]))
    image_groups = groupby(images, key=lambda i: (i[4], i[5], i[6]))

    processed_images = []

    for group_key, group_images in image_groups:
        images = list(group_images)
        output_image = np.zeros((750, 250, 3), dtype="uint8")


        for idx, image_data in enumerate(images):
            try:
                image = cv2.imread(image_data[0])
                image = cv2.resize(image, (100, 100))

                if idx == 0:
                    output_image[0:100, 0:100, :] = image
                if idx == 1:
                    output_image[100:200, 0:100, :] = image
                if idx == 2:
                    output_image[200:300, 0:100, :] = image

            except:
                pass

        processed_images.append((*group_key, output_image))

    return processed_images

def filter_image(image):
    full_path, filename, camera_name, date_epoch, date_str, date_hour, date_minute = image

    if date_str < '2022-02-10':
        return False

    if date_str > '2022-03-06':
        return False

    if date_minute % 5 != 0:
        return False

    return True

def import_all():
    path = 'data/cameras'
    images = [parse_filename(path, f) for f in listdir(path)]

    print(f'Total: {len(images)} images')

    images = list(filter(filter_image, images))

    print(f'After filtering: {len(images)} images')
    images = process_images(images)

    print(f'After processing: {len(images)} images')

    df = pd.DataFrame(images, columns=['Date', 'Hour', 'Minute', 'Image'])

    print(df.head(19))

    return df
