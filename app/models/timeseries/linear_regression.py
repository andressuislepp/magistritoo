import matplotlib.pyplot as plt
import numpy as np
import pandas as pd
from dateutil import parser
from scalecast.Forecaster import Forecaster
import seaborn as sns
from sklearn.preprocessing import MinMaxScaler
from sklearn.linear_model import LinearRegression

import utils
from models import config

def handle_sequence(df, n_outputs):
    df = pd.DataFrame(df, columns=['TN_Noise_Level_DB', 'Minute_Of_Day'])
    df['TN_Noise_Level_DB'] = pd.to_numeric(df['TN_Noise_Level_DB'])

    reg = LinearRegression().fit(df.Minute_Of_Day.to_numpy().reshape(-1, 1), df.TN_Noise_Level_DB.to_numpy())

    last = df['Minute_Of_Day'].iloc[-1]

    predictions = reg.predict(np.array([(last + (n_outputs * 5)) % 1440]).reshape(1, -1))

    # plt.scatter(df.Minute_Of_Day.to_numpy().reshape(-1, 1), df.TN_Noise_Level_DB.to_numpy(), color='g')
    # plt.plot(df.Minute_Of_Day.to_numpy().reshape(-1, 1), reg.predict(df.Minute_Of_Day.to_numpy().reshape(-1, 1)), color='k')
    #
    # plt.show()

    return predictions[0]


def handle_linear_regression(scaler, df, n_lag, n_outputs, test_size):
    df2 = preprocess(df)

    print("-> Constructing train/test split")

    print("-> Preprocessing")
    data = df2

    n_features = data.shape[1]
    n_rows = data.shape[0]

    # Splitting into train and test sets
    train = data[0:int(n_rows * (1 - test_size))]
    test = data[int(n_rows * (1 - test_size)):]

    # Creating the final scaled frame
    data_s = pd.concat([pd.DataFrame(train), pd.DataFrame(test)])

    timestamps = df['Datetime'].to_numpy()
    x, y = utils.timeseries_to_supervised(data_s.values, timestamps, lag=n_lag, n_ahead=n_outputs, target_index=0)

    train_x, train_y = x[0:int(x.shape[0] * (1 - test_size))], y[0:int(x.shape[0] * (1 - test_size))]
    test_x, test_y = x[int(x.shape[0] * (1 - test_size)):], y[int(x.shape[0] * (1 - test_size)):]

    print(f'Train: x: {train_x.shape}, y: {train_y.shape}')
    print(f'Test: x: {test_x.shape}, y: {test_y.shape}')

    predictions = np.array([handle_sequence(p, n_outputs) for p in test_x])

    print(f'Prediction: x: {predictions.shape}, y: {predictions.shape}')

    scaled_test_y = scaler.inverse_transform(np.array(test_y).reshape(-1, 1))
    scaled_predictions = scaler.inverse_transform(np.array(predictions).reshape(-1, 1))

    utils.log_result('linear_regression', None, n_lag, '-', n_outputs,
                     scaled_test_y, scaled_predictions, None, 0)


def preprocess(df):
    target = config.target_variable
    target = df[target]
    cont = np.hstack([target, df[['Minute_Of_Day']]])

    return cont
