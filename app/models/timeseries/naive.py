import matplotlib.pyplot as plt
import numpy as np
import pandas as pd
from sklearn.metrics import mean_squared_error
from sklearn.preprocessing import LabelBinarizer
from sklearn.preprocessing import MinMaxScaler

import utils
from models import config


def handle_naive(scaler, df, n_lag, n_outputs, test_size):


    df2 = preprocess(df)

    print("-> Constructing train/test split")

    print("-> Preprocessing")
    data = df2

    n_features = data.shape[1]
    n_rows = data.shape[0]

    # Splitting into train and test sets
    train = data[0:int(n_rows * (1 - test_size))]
    test = data[int(n_rows * (1 - test_size)):]

    # Creating the final scaled frame
    data_s = pd.concat([pd.DataFrame(train), pd.DataFrame(test)])

    timestamps = df['Datetime'].to_numpy()
    x, y = utils.timeseries_to_supervised(data_s.values, timestamps, lag=n_lag, n_ahead=n_outputs, target_index=0)


    train_x, train_y = x[0:int(x.shape[0] * (1 - test_size))], y[0:int(x.shape[0] * (1 - test_size))]
    test_x, test_y = x[int(x.shape[0] * (1 - test_size)):], y[int(x.shape[0] * (1 - test_size)):]

    print(f'Train: x: {train_x.shape}, y: {train_y.shape}')
    print(f'Test: x: {test_x.shape}, y: {test_y.shape}')

    naive = np.array([p[len(p) - 1, 0].flatten() for p in test_x])

    print(f'Prediction: x: {naive.shape}, y: {naive.shape}')

    scaled_test_y = scaler.inverse_transform(np.array(test_y).reshape(-1, 1))
    scaled_naive = scaler.inverse_transform(np.array(naive).reshape(-1, 1))

    print(f'RMSE - naive: {np.sqrt(mean_squared_error(scaled_test_y, scaled_naive))}')

    utils.log_result('naive', None, n_lag, '-', n_outputs,
                     scaled_test_y, scaled_naive, None, 0)


def preprocess(df):
    target = config.target_variable
    target = df[target]
    target = np.hstack([target])

    return target
