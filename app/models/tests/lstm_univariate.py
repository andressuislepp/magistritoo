import matplotlib.pyplot as plt
import numpy as np
import pandas as pd
from sklearn.metrics import mean_squared_error
from sklearn.preprocessing import MinMaxScaler
from tensorflow.keras.layers import Bidirectional
from tensorflow.keras.layers import Dense
from tensorflow.keras.layers import LSTM
from tensorflow.keras.models import Sequential

import utils


def create(n_lag, n_features, n_output):
    model = Sequential()
    model.add(LSTM(100, activation='relu', return_sequences=True, input_shape=(n_lag, n_features)))
    model.add(LSTM(50, activation='relu', return_sequences=False))
    # model.add(Bidirectional(LSTM(128, return_sequences=True)))
    # model.add(Bidirectional(LSTM(64, return_sequences=True)))
    # model.add(Bidirectional(LSTM(32, return_sequences=True)))
    # model.add(Bidirectional(LSTM(16, return_sequences=True)))
    # model.add(LSTM(8, return_sequences=False))
    model.add(Dense(n_output))

    return model


def handle_lstm(df, n_lag, n_epochs, n_output, test_size):
    df.info()

    (df2, scaler) = preprocess(df)

    print("-> Constructing train/test split")

    print("-> Preprocessing")
    data = df2

    n_features = data.shape[1]
    n_rows = data.shape[0]

    # Splitting into train and test sets
    train = data[0:int(n_rows * (1 - test_size))]
    test = data[int(n_rows * (1 - test_size)):]

    # Creating the final scaled frame
    data_s = pd.concat([pd.DataFrame(train), pd.DataFrame(test)])

    timestamps = df['Datetime'].to_numpy()
    x, y = utils.timeseries_to_supervised(data_s.values, timestamps, lag=n_lag, n_ahead=n_output, target_index=0)

    y_scaled_back = scaler.inverse_transform(y)

    train_x, train_y = x[0:int(x.shape[0] * (1 - test_size))], y[0:int(x.shape[0] * (1 - test_size))]
    test_x, test_y = x[int(x.shape[0] * (1 - test_size)):], y[int(x.shape[0] * (1 - test_size)):]

    print(f'Train: x: {train_x.shape}, y: {train_y.shape}')
    print(f'Test: x: {test_x.shape}, y: {test_y.shape}')

    model = create(n_lag, n_features, n_output)
    model.compile(optimizer='adam', loss='mae', metrics=['mae'])

    # train the model
    print("-> Training")
    history = model.fit(x=train_x, y=train_y,
                        validation_data=(test_x, test_y),
                        epochs=n_epochs, batch_size=36)

    print("-> Predicting noise levels")
    predictions = model.predict(test_x)
    print(f'Predictions shape: {predictions.shape}')

    predictions_y = np.array([p[0:n_lag] for p in predictions])

    print("Predictions:")
    print(predictions_y[0:3])

    scaled_predictions_y = scaler.inverse_transform(predictions_y.reshape(-1, 1))
    scaled_test_y = scaler.inverse_transform(test_y.reshape(-1, 1))

    print(f'RMSE - asdf: {np.sqrt(mean_squared_error(scaled_test_y, scaled_predictions_y))}')

    print("True_scaled:")
    print(scaled_test_y[0:3])
    print("Predictions_scaled:")
    print(scaled_predictions_y[0:3])
    # print((scaled_test_y - scaled_predictions_y)[0:3])

    utils.log_result('lstm_univariate', n_lag, n_epochs, n_output,
                     np.sqrt(mean_squared_error(scaled_test_y, scaled_predictions_y)),
                     scaled_test_y,
                     scaled_predictions_y)


def preprocess(df):
    target = ['TN_Noise_Level_DB']

    target_scaler = MinMaxScaler(feature_range=(0, 1))
    target = target_scaler.fit_transform(df[target])

    res = np.hstack([target])

    return res, target_scaler
