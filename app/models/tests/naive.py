import matplotlib.pyplot as plt
import numpy as np
import pandas as pd
from sklearn.metrics import mean_squared_error
from sklearn.preprocessing import LabelBinarizer
from sklearn.preprocessing import MinMaxScaler

import utils


def handle_naive(df, n_lag, n_outputs, test_size):


    (df2, scaler) = preprocess(df)

    print("-> Constructing train/test split")

    print("-> Preprocessing")
    data = df2

    n_features = data.shape[1]
    n_rows = data.shape[0]

    # Splitting into train and test sets
    train = data[0:int(n_rows * (1 - test_size))]
    test = data[int(n_rows * (1 - test_size)):]

    # Creating the final scaled frame
    data_s = pd.concat([pd.DataFrame(train), pd.DataFrame(test)])

    timestamps = df['Datetime'].to_numpy()
    x, y = utils.timeseries_to_supervised(data_s.values, timestamps, lag=n_lag, n_ahead=n_outputs, target_index=0)


    train_x, train_y = x[0:int(x.shape[0] * (1 - test_size))], y[0:int(x.shape[0] * (1 - test_size))]
    test_x, test_y = x[int(x.shape[0] * (1 - test_size)):], y[int(x.shape[0] * (1 - test_size)):]

    print(f'Train: x: {train_x.shape}, y: {train_y.shape}')
    print(f'Test: x: {test_x.shape}, y: {test_y.shape}')

    naive = [p[len(p) - 1, 0].flatten() for p in test_x]

    scaled_test_y = scaler.inverse_transform(test_y)
    scaled_naive = scaler.inverse_transform(np.array(naive).reshape(-1, 1))

    print(f'RMSE - naive: {np.sqrt(mean_squared_error(scaled_test_y, scaled_naive))}')

    utils.log_result('naive', n_lag, '-',
                     np.sqrt(mean_squared_error(scaled_test_y, scaled_naive)),
                     scaled_test_y,
                     scaled_naive)


def preprocess(df):
    df['Hour_cos'] = [np.cos(x * (2 * np.pi / 24)) for x in df['Hour']]
    df['Hour_sin'] = [np.sin(x * (2 * np.pi / 24)) for x in df['Hour']]

    target = ['TN_Noise_Level_DB']

    continuous = ['IW_Temp_C', 'IW_Wind_Temp_C', 'IW_Rain_MMH', 'TT_P1_Speed_Diff_Kmh', 'TT_P1_Travel_Time_Diff_Sec',
                  'TT_P2_Speed_Diff_Kmh', 'TT_P2_Travel_Time_Diff_Sec', 'Hour_sin', 'Hour_cos']

    target_scaler = MinMaxScaler(feature_range=(0, 1))
    target = target_scaler.fit_transform(df[target])

    scaler = MinMaxScaler(feature_range=(0, 1))
    continuous = scaler.fit_transform(df[continuous])

    categorical = ['TT_P1_Road_Type_Class']

    # one-hot encoding, all output features are now in the range [0, 1])
    weather_binarizer = LabelBinarizer().fit(df[categorical])
    categorical = weather_binarizer.transform(df[categorical])

    categorical2 = ['IW_Weather_Cloudiness_Class']

    # one-hot encoding, all output features are now in the range [0, 1])
    weather_binarizer = LabelBinarizer().fit(df[categorical2])
    categorical2 = weather_binarizer.transform(df[categorical2])

    categorical3 = ['IW_Weather_Coldness_Class']

    # one-hot encoding, all output features are now in the range [0, 1])
    weather_binarizer = LabelBinarizer().fit(df[categorical3])
    categorical3 = weather_binarizer.transform(df[categorical3])

    # construct our training and testing data points by concatenating
    # the categorical features with the continuous features

    res = np.hstack(
        [target, continuous, categorical, categorical2, categorical3])

    # res = np.hstack([target])
    return res, target_scaler
