import sys

import matplotlib.pyplot as plt
import numpy as np
import pandas as pd
from tensorflow.keras import Model
from tensorflow.keras.layers import concatenate, Reshape
from sklearn.metrics import mean_squared_error
from sklearn.preprocessing import LabelBinarizer
from sklearn.preprocessing import MinMaxScaler
from sklearn.preprocessing import LabelEncoder
from tensorflow.keras.layers import Bidirectional
from tensorflow.keras.layers import Dense
from tensorflow.keras.layers import Embedding
from tensorflow.keras.layers import Input
from tensorflow.keras.layers import Flatten
from tensorflow.keras.layers import LSTM
from tensorflow.keras.layers import TimeDistributed
from tensorflow.keras.models import Sequential

import utils


def create(model, n_lag, n_features):
    model.add(Bidirectional(LSTM(100, activation='relu', return_sequences=False, input_shape=(n_lag, n_features))))
    model.add(Dense(1))

    return model


def create_embeddings(input_dim, input_length):
    embedding_size = 3
    # Jeremy Howard provides the following rule of thumb; embedding size = min(50, number of categories/2).
    # embedding size = min(50, number of categories/2)

    model = Sequential()
    model.add(Embedding(input_dim=input_dim, output_dim=embedding_size, input_length=input_length, name="embedding"))
    model.add(Flatten())
    model.add(Dense(50, activation="relu"))
    model.add(Dense(15, activation="relu"))
    model.add(Dense(1))

    return model

# + embeddings
def handle_lstm_embed(df, n_lag, n_epochs, n_outputs, test_size):
    df.info()

    (df_continious, df_categorical, scaler) = preprocess(df)

    n_features = df_continious.shape[1]

    n_category = 1  # number of categorical features
    print(f'n_category: {n_category}')
    print(f'n_features: {n_features}')

    categorical_length = len(df_categorical)
    categorical_dim = len(np.unique(df_categorical))

    cat_size = [categorical_length]  # number of categories in each categorical feature
    cat_embd_dim = [categorical_dim]  # embedding dimension for each categorical feature

    numerical_input = Input(shape=(n_lag, n_features), name='continious_input')
    cat_inputs = []
    for i in range(n_category):
        cat_inputs.append(Input(shape=(n_lag, 1), name='categorical' + str(i + 1) + '_input'))

    cat_embedded = []
    for i in range(n_category):
        embed = Embedding(cat_size[i], cat_embd_dim[i])(cat_inputs[i])
        cat_embedded.append(embed)

    cat_merged = concatenate(cat_embedded)
    cat_merged = Reshape((n_lag, -1))(cat_merged)
    merged = concatenate([numerical_input, cat_merged])
    x = LSTM(100)(merged)
    # model.add(Bidirectional(LSTM(100, activation='relu', return_sequences=False, input_shape=(n_lag, n_features))))
    x = Dense(n_outputs)(x)

    model = Model([numerical_input] + cat_inputs, x)
    model.summary()

    print("-> Constructing train/test split")

    print("-> Preprocessing")
    data = df_continious

    n_rows = data.shape[0]

    # Splitting into train and test sets
    train = data[0:int(n_rows * (1 - test_size))]
    test = data[int(n_rows * (1 - test_size)):]

    train_cat = df_categorical[0:int(n_rows * (1 - test_size))]
    test_cat = df_categorical[int(n_rows * (1 - test_size)):]

    # Creating the final scaled frame
    data_s = pd.concat([pd.DataFrame(train), pd.DataFrame(test)])
    data_cat = pd.concat([pd.DataFrame(train_cat), pd.DataFrame(test_cat)])

    x, y = utils.timeseries_to_supervised(data_s.values, lag=n_lag, n_ahead=n_outputs, target_index=0)
    x_cat, y_cat = utils.timeseries_to_supervised(data_cat.values, lag=n_lag, n_ahead=n_outputs, target_index=0)

    print(f'x: {x.shape}, y: {y.shape}')

    train_x, train_y = x[0:int(x.shape[0] * (1 - test_size))], y[0:int(x.shape[0] * (1 - test_size))]
    test_x, test_y = x[int(x.shape[0] * (1 - test_size)):], y[int(x.shape[0] * (1 - test_size)):]

    cat_train_x, cat_train_y = x_cat[0:int(x_cat.shape[0] * (1 - test_size))], y_cat[0:int(x_cat.shape[0] * (1 - test_size))]
    cat_test_x, cat_test_y = x_cat[int(x_cat.shape[0] * (1 - test_size)):], y_cat[int(x.shape[0] * (1 - test_size)):]

    print(f'Train: x: {train_x.shape}, y: {train_y.shape}')
    print(f'Test: x: {test_x.shape}, y: {test_y.shape}')

    print(f'CAT Train: x: {cat_train_x.shape}, y: {cat_train_y.shape}')
    print(f'CAT Test: x: {cat_test_x.shape}, y: {cat_test_y.shape}')

    model.compile(optimizer='adam', loss='mae', metrics=['mae'])


    # train the model
    print("-> Training")
    history = model.fit(x=[train_x, cat_train_x], y=train_y,
                        validation_data=([test_x, cat_test_x], test_y),
                        epochs=n_epochs, batch_size=4)

    print("-> Predicting noise levels")
    predictions = model.predict([test_x, cat_test_x])
    print(f'Predictions shape: {predictions.shape}')

    predictions_y = np.array([p[0] for p in predictions]).reshape(-1, 1)

    print("Predictions:")
    print(predictions_y[0:3])

    scaled_predictions_y = scaler.inverse_transform(predictions_y)
    scaled_test_y = scaler.inverse_transform(test_y)

    print(f'RMSE - asdf: {np.sqrt(mean_squared_error(scaled_test_y, scaled_predictions_y))}')

    print("True_scaled:")
    print(scaled_test_y[0:3])
    print("Predictions_scaled:")
    print(scaled_predictions_y[0:3])
    # print((scaled_test_y - scaled_predictions_y)[0:3])

    diff = predictions.flatten() - test_y
    percent_diff = (diff / test_y) * 100
    abs_percent_diff = np.abs(percent_diff)
    # compute the mean and standard deviation of the absolute percentage
    # difference
    mean = np.mean(abs_percent_diff)
    std = np.std(abs_percent_diff)

    print("-> avg. noise level: {}, std noise level: {}".format(
        df["TN_Noise_Level_DB"].mean(),
        df["TN_Noise_Level_DB"].std()))
    print("-> mean: {:.2f}%, std: {:.2f}%".format(mean, std))

    utils.log_result('lstm_embed', n_lag, n_epochs,
                     np.sqrt(mean_squared_error(scaled_test_y, scaled_predictions_y)),
                     scaled_test_y,
                     scaled_predictions_y)



def preprocess(df):
    df['Hour_cos'] = [np.cos(x * (2 * np.pi / 24)) for x in df['Hour']]
    df['Hour_sin'] = [np.sin(x * (2 * np.pi / 24)) for x in df['Hour']]

    target = ['TN_Noise_Level_DB']

    continuous = ['IW_Temp_C', 'IW_Wind_Temp_C', 'IW_Rain_MMH', 'TT_P1_Speed_Diff_Kmh', 'TT_P1_Travel_Time_Diff_Sec',
                  'TT_P2_Speed_Diff_Kmh', 'TT_P2_Travel_Time_Diff_Sec', 'Hour_cos', 'Hour_sin']

    target_scaler = MinMaxScaler(feature_range=(0, 1))
    target = target_scaler.fit_transform(df[target])

    scaler = MinMaxScaler(feature_range=(0, 1))
    continuous = scaler.fit_transform(df[continuous])

    # categorical = ['TT_P1_Road_Type_Class']
    #
    # # one-hot encoding, all output features are now in the range [0, 1])
    # weather_binarizer = LabelEncoder().fit(df[categorical])
    # categorical = weather_binarizer.transform(df[categorical])

    categorical2 = ['IW_Weather_Cloudiness_Class']

    # one-hot encoding, all output features are now in the range [0, 1])
    weather_binarizer = LabelEncoder().fit(df[categorical2])
    categorical2 = weather_binarizer.transform(df[categorical2])

    # categorical3 = ['IW_Weather_Coldness_Class']
    #
    # # one-hot encoding, all output features are now in the range [0, 1])
    # weather_binarizer = LabelEncoder().fit(df[categorical3])
    # categorical3 = weather_binarizer.transform(df[categorical3])

    cont = np.hstack([target, continuous])
    cat = np.hstack([categorical2])

    return cont, cat, target_scaler
