import matplotlib.pyplot as plt
import numpy as np
import pandas as pd
from sklearn.metrics import mean_squared_error
from sklearn.preprocessing import LabelEncoder
from sklearn.preprocessing import MinMaxScaler
from tensorflow.keras import Model
from tensorflow.keras.layers import Dense
from tensorflow.keras.layers import Embedding
from tensorflow.keras.layers import Input
from tensorflow.keras.layers import LSTM, Bidirectional, Conv1D, Flatten, BatchNormalization, MaxPooling1D
from tensorflow.keras.layers import concatenate, Reshape, Dropout

import utils
from models.layers.Time2Vec import Time2Vec
from models.layers.Weather2Vec import Weather2Vec


def handle_lstm_2vec(df, n_lag, n_epochs, n_outputs, test_size):
    df.info()

    (df_continious, scaler) = preprocess(df)

    n_features = df_continious.shape[1]

    numerical_input = Input(shape=(n_lag, n_features), name='continious_input')

    x = Conv1D(filters=100, kernel_size=1, activation='relu')(numerical_input)
    x = MaxPooling1D(pool_size=2)(x)
    # x = Dropout(0.2)(x)
    x = Conv1D(filters=100, kernel_size=1, activation='relu')(x)
    x = MaxPooling1D(pool_size=2)(x)
    # x = Dropout(0.2)(x)
    x = Conv1D(filters=100, kernel_size=1, activation='relu')(x)
    x = MaxPooling1D(pool_size=2)(x)
    # x = Flatten()(x)
    x = Bidirectional(LSTM(100, activation='relu', return_sequences=True))(x)
    x = Bidirectional(LSTM(50, activation='relu', return_sequences=False))(x)
    x = Dense(n_outputs)(x)

    model = Model([numerical_input], x)
    model.summary()

    print("-> Constructing train/test split")

    print("-> Preprocessing")
    data = df_continious

    n_rows = data.shape[0]

    # Splitting into train and test sets
    train = data[0:int(n_rows * (1 - test_size))]
    test = data[int(n_rows * (1 - test_size)):]

    # Creating the final scaled frame
    data_s = pd.concat([pd.DataFrame(train), pd.DataFrame(test)])

    timestamps = df['Datetime'].to_numpy()

    x, y = utils.timeseries_to_supervised(data_s.values, timestamps, lag=n_lag, n_ahead=n_outputs, target_index=0)


    print(f'x: {x.shape}, y: {y.shape}')

    train_x, train_y = x[0:int(x.shape[0] * (1 - test_size))], y[0:int(x.shape[0] * (1 - test_size))]
    test_x, test_y = x[int(x.shape[0] * (1 - test_size)):], y[int(x.shape[0] * (1 - test_size)):]

    print(f'Train: x: {train_x.shape}, y: {train_y.shape}')
    print(f'Test: x: {test_x.shape}, y: {test_y.shape}')

    model.compile(optimizer='adam', loss='mae', metrics=['mae'])

    # train the model
    print("-> Training")
    history = model.fit(x=[train_x], y=train_y,
                        validation_data=([test_x], test_y),
                        epochs=n_epochs, batch_size=4)

    print("-> Predicting noise levels")
    predictions = model.predict([test_x])
    print(f'Predictions shape: {predictions.shape}')

    predictions_y = np.array([p[0:n_lag] for p in predictions])

    print("Predictions:")
    print(predictions_y[0:3])

    scaled_predictions_y = scaler.inverse_transform(predictions_y.reshape(-1, 1))
    scaled_test_y = scaler.inverse_transform(test_y.reshape(-1, 1))

    print(f'RMSE - asdf: {np.sqrt(mean_squared_error(scaled_test_y, scaled_predictions_y))}')

    print("True_scaled:")
    print(scaled_test_y[0:3])
    print("Predictions_scaled:")
    print(scaled_predictions_y[0:3])
    # print((scaled_test_y - scaled_predictions_y)[0:3])

    utils.log_result('aarh:lstm_uni', n_lag, n_epochs, n_outputs,
                     np.sqrt(mean_squared_error(scaled_test_y, scaled_predictions_y)),
                     scaled_test_y,
                     scaled_predictions_y)

def preprocess(df):
    target = ['VehicleCount']

    continuous = ['VehicleCount']

    target_scaler = MinMaxScaler(feature_range=(0, 1))
    target = target_scaler.fit_transform(df[target])

    scaler = MinMaxScaler(feature_range=(0, 1))
    continuous = scaler.fit_transform(df[continuous])

    cont = np.hstack([target, continuous])

    return cont, target_scaler
