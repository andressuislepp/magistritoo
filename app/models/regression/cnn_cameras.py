from tensorflow.keras.layers import Dense
from tensorflow.keras import Input
from tensorflow.keras import Model
from tensorflow.keras.layers import Conv2D
from tensorflow.keras.layers import Activation
from tensorflow.keras.layers import BatchNormalization
from tensorflow.keras.layers import MaxPooling2D
from tensorflow.keras.layers import Flatten
from tensorflow.keras.layers import TimeDistributed
from tensorflow.keras.layers import Dropout
import numpy as np
import pandas as pd
from sklearn.model_selection import train_test_split
from tensorflow.keras.optimizers import Adam
from tensorflow.keras.backend import concatenate
from tensorflow.keras.layers import Dense
from tensorflow.keras import Model

import utils


def create(width, height, depth, filters=(16, 32, 64), regress=False):
    input_shape = (height, width, depth)
    channel_dim = -1

    inputs = Input(shape=input_shape)

    for (i, f) in enumerate(filters):
        if i == 0:
            x = inputs

        # CONV => RELU > BN => POOL
        x = Conv2D(f, (3, 3), padding="same")(x)
        x = Activation("relu")(x)
        x = BatchNormalization(axis=channel_dim)(x)
        x = MaxPooling2D(pool_size=(2, 2))(x)

    # flatten the volume, then FC => RELU => BN => DROPOUT
    x = Flatten()(x)
    x = Dense(16)(x)
    x = Activation("relu")(x)
    x = BatchNormalization(axis=channel_dim)(x)
    x = Dropout(0.5)(x)

    # apply another FC layer, this one to match the number of nodes
    # coming out of the MLP
    x = Dense(4)(x)
    x = Activation("relu")(x)

    if regress:
        x = Dense(1, activation="linear")(x)

    model = Model(inputs, x)

    return model


def handle_cnn_cameras(df, n_epochs, test_size):
    print("-> Loading images & data")
    df = df.loc[df['TN_Noise_Level_DB'].notnull()]
    df = df.loc[df['Image'].notnull()]

    images = df['Image'].to_numpy()
    images = np.array(list(images), dtype=np.float64)
    images = images / 255.

    (train, test, train_images, test_images) = train_test_split(df, images, test_size=test_size, random_state=42)
    max_noise = train["TN_Noise_Level_DB"].max()
    train_y = train["TN_Noise_Level_DB"].to_numpy() / max_noise
    test_y = test["TN_Noise_Level_DB"].to_numpy() / max_noise

    print("-> Creating model")

    model = create(256, 768, 3, regress=True)
    opt = Adam(lr=1e-3, decay=1e-3 / 200)
    model.compile(loss="mean_absolute_percentage_error", optimizer=opt)

    print("-> Training")
    model.fit(x=train_images, y=train_y,
              validation_data=(test_images, test_y), epochs=n_epochs, batch_size=8)

    print("-> Predicting noise levels using images only")
    predictions = model.predict(test_images)

    diff = predictions.flatten() - test_y
    percent_diff = (diff / test_y) * 100
    abs_percent_diff = np.abs(percent_diff)
    # compute the mean and standard deviation of the absolute percentage
    # difference
    mean = np.mean(abs_percent_diff)
    std = np.std(abs_percent_diff)

    print("-> avg. noise level: {}, std noise level: {}".format(
        df["TN_Noise_Level_DB"].mean(),
        df["TN_Noise_Level_DB"].std()))
    print("-> mean: {:.2f}%, std: {:.2f}%".format(mean, std))

    utils.log_regression_result('images', n_epochs, mean)

