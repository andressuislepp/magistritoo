import numpy as np
import tensorflow.keras.losses
from sklearn.model_selection import train_test_split
from sklearn.preprocessing import LabelBinarizer
from sklearn.preprocessing import MinMaxScaler
from tensorflow.keras import Input
from tensorflow.keras import Model
from tensorflow.keras.backend import concatenate
from tensorflow.keras.layers import Activation
from tensorflow.keras.layers import BatchNormalization
from tensorflow.keras.layers import Conv2D
from tensorflow.keras.layers import Dense
from tensorflow.keras.layers import Dropout
from tensorflow.keras.layers import Flatten
from tensorflow.keras.layers import MaxPooling2D
from tensorflow.keras.models import Sequential
from tensorflow.keras.optimizers import Adam

import utils


def create_cnn(width, height, depth, filters=(16, 32, 64), regress=False):
    input_shape = (height, width, depth)
    channel_dim = -1

    inputs = Input(shape=input_shape)

    for (i, f) in enumerate(filters):
        if i == 0:
            x = inputs

        # CONV => RELU > BN => POOL
        x = Conv2D(f, (3, 3), padding="same")(x)
        x = Activation("relu")(x)
        x = BatchNormalization(axis=channel_dim)(x)
        x = MaxPooling2D(pool_size=(2, 2))(x)

    # flatten the volume, then FC => RELU => BN => DROPOUT
    x = Flatten()(x)
    x = Dense(16)(x)
    x = Activation("relu")(x)
    x = BatchNormalization(axis=channel_dim)(x)
    x = Dropout(0.5)(x)

    # apply another FC layer, this one to match the number of nodes
    # coming out of the MLP
    x = Dense(4)(x)
    x = Activation("relu")(x)

    if regress:
        x = Dense(1, activation="linear")(x)

    model = Model(inputs, x)

    return model


def create(dim, regress=False):
    # define our MLP network
    model = Sequential()
    model.add(Dense(8, input_dim=dim, activation="relu"))
    model.add(Dense(4, activation="relu"))
    # check to see if the regression node should be added
    if regress:
        model.add(Dense(1, activation="linear"))
    # return our model
    return model


def preprocess(df, train, test):
    continuous = ['TT_P1_Speed_Diff_Kmh', 'TT_P1_Travel_Time_Diff_Sec',
                  'TT_P2_Speed_Diff_Kmh', 'TT_P2_Travel_Time_Diff_Sec',
                  'IW_Temp_C', 'IW_Wind_Temp_C', 'IW_Rain_MMH']

    cs = MinMaxScaler()
    train_continuous = cs.fit_transform(train[continuous])
    test_continuous = cs.transform(test[continuous])

    categorical = ['TT_P1_Road_Type_Class']

    # one-hot encoding, all output features are now in the range [0, 1])
    weather_binarizer = LabelBinarizer().fit(df[categorical])
    train_categorical = weather_binarizer.transform(train[categorical])
    test_categorical = weather_binarizer.transform(test[categorical])

    categorical2 = ['IW_Weather_Cloudiness_Class']

    # one-hot encoding, all output features are now in the range [0, 1])
    weather_binarizer = LabelBinarizer().fit(df[categorical2])
    train_categorical2 = weather_binarizer.transform(train[categorical2])
    test_categorical2 = weather_binarizer.transform(test[categorical2])

    categorical3 = ['IW_Weather_Cloudiness_Class']

    # one-hot encoding, all output features are now in the range [0, 1])
    weather_binarizer = LabelBinarizer().fit(df[categorical3])
    train_categorical3 = weather_binarizer.transform(train[categorical3])
    test_categorical3 = weather_binarizer.transform(test[categorical3])

    categorical4 = ['Hour']

    weather_binarizer = LabelBinarizer().fit(df[categorical4])
    train_categorical4 = weather_binarizer.transform(train[categorical4])
    test_categorical4 = weather_binarizer.transform(test[categorical4])

    categorical5 = ['Day_Of_Week']

    weather_binarizer = LabelBinarizer().fit(df[categorical5])
    train_categorical5 = weather_binarizer.transform(train[categorical5])
    test_categorical5 = weather_binarizer.transform(test[categorical5])

    # construct our training and testing data points by concatenating
    # the categorical features with the continuous features

    train_x = np.hstack(
        [train_categorical, train_categorical2, train_categorical3, train_categorical4, train_categorical5,
         train_continuous])
    test_x = np.hstack(
        [test_categorical, test_categorical2, test_categorical3, test_categorical4, test_categorical5, test_continuous])

    # train_x = np.hstack([train_continuous])
    # test_x = np.hstack([test_continuous])

    # train_x = np.hstack([train_categorical, train_categorical2])
    # test_x = np.hstack([test_categorical, test_categorical2])

    return train_x, test_x


def handle_mixed(df, n_epochs, test_size):
    df = df.loc[df['TN_Noise_Level_DB'].notnull()]
    df = df.loc[df['Image'].notnull()]
    df = df.loc[df['Minute'] % 5 == 0]
    df = df.loc[df['TT_P1_Speed_Diff_Kmh'].notnull()]
    df = df.loc[df['TT_P1_Travel_Time_Diff_Sec'].notnull()]
    df = df.loc[df['TT_P2_Speed_Diff_Kmh'].notnull()]
    df = df.loc[df['TT_P2_Travel_Time_Diff_Sec'].notnull()]
    df = df.loc[df['IW_Temp_C'].notnull()]
    df = df.loc[df['IW_Wind_Temp_C'].notnull()]
    df = df.loc[df['IW_Rain_MMH'].notnull()]

    images = df['Image'].to_numpy()
    images = np.array(list(images), dtype=np.float64)
    images = images / 255.

    print("-> Constructing train/test split")
    (train, test, train_images, test_images) = train_test_split(df, images, test_size=test_size, random_state=42)
    max_noise = train["TN_Noise_Level_DB"].max()
    train_y = train["TN_Noise_Level_DB"] / max_noise
    test_y = test["TN_Noise_Level_DB"] / max_noise

    print(f'Train: {train.shape}')
    print(f'Test: {test.shape}')

    print("-> Preprocessing")
    (train_x, test_x) = preprocess(df, train, test)

    print("-> Creating models")

    model_mlp = create(train_x.shape[1], regress=False)
    model_cnn = create_cnn(256, 768, 3, regress=False)

    combined_input = concatenate([model_mlp.output, model_cnn.output])
    # our final FC layer head will have two dense layers, the final one
    # being our regression head
    x = Dense(4, activation="relu")(combined_input)
    x = Dense(1, activation="linear")(x)
    # our final model will accept categorical/numerical data on the MLP
    # input and images on the CNN input, outputting a single value (the
    # predicted price of the house)
    model = Model(inputs=[model_mlp.input, model_cnn.input], outputs=x)

    opt = Adam(lr=1e-3, decay=1e-3 / 200)
    model.compile(loss="mean_absolute_percentage_error", optimizer=opt)

    print("-> Training")
    m = model.fit(
        x=[train_x, train_images], y=train_y,
        validation_data=([test_x, test_images], test_y),
        epochs=n_epochs, batch_size=8
    )

    predictions = model.predict([test_x, test_images])

    mape = tensorflow.keras.losses.mean_absolute_percentage_error(test_y, predictions).numpy()

    print(mape)


    diff = predictions.flatten() - test_y
    percent_diff = (diff / test_y) * 100
    abs_percent_diff = np.abs(percent_diff)
    # compute the mean and standard deviation of the absolute percentage
    # difference
    mean = np.mean(abs_percent_diff)
    std = np.std(abs_percent_diff)

    print("-> avg. noise level: {}, std noise level: {}".format(
        df["TN_Noise_Level_DB"].mean(),
        df["TN_Noise_Level_DB"].std()))
    print("-> mean: {:.2f}%, std: {:.2f}%".format(mean, std))

    utils.log_regression_result('mixed', n_epochs, mean)
