import time

import numpy as np
import pandas as pd
from sklearn.preprocessing import LabelEncoder
from sklearn.preprocessing import MinMaxScaler
from tensorflow.keras import Input
from tensorflow.keras import Model
from tensorflow.keras.layers import Activation
from tensorflow.keras.layers import BatchNormalization
from tensorflow.keras.layers import Conv2D
from tensorflow.keras.layers import Dense
from tensorflow.keras.layers import Dropout
from tensorflow.keras.layers import Embedding
from tensorflow.keras.layers import Flatten
from tensorflow.keras.layers import MaxPooling2D
from tensorflow.keras.layers import TimeDistributed
from tensorflow.keras.layers import concatenate, Reshape

import utils
from models import lstm
from models.layers.Time2Vec import Time2Vec
from models.layers.Weather2Vec import Weather2Vec


def create_cnn(n_lag, width, height, depth, filters=(16, 32, 64)):
    input_shape = (height, width, depth)
    channel_dim = -1

    inputs = Input(shape=input_shape)

    for (i, f) in enumerate(filters):
        if i == 0:
            x = inputs

        # CONV => RELU > BN => POOL
        x = Conv2D(f, (3, 3), padding="same")(x)
        x = Activation("relu")(x)
        x = BatchNormalization(axis=channel_dim)(x)
        x = MaxPooling2D(pool_size=(2, 2))(x)
    x = Flatten()(x)
    x = Dense(16)(x)
    x = Activation("relu")(x)
    x = BatchNormalization(axis=channel_dim)(x)
    x = Dropout(0.3)(x)

    x = Dense(1, activation="linear")(x)

    model = Model(inputs, x)

    td_input = Input(shape=(n_lag, height, width, depth), name="images")

    td = TimeDistributed(model)(td_input)

    # inputs = tf.keras.Input(shape=(10, 128, 128, 3))
    # >> > conv_2d_layer = tf.keras.layers.Conv2D(64, (3, 3))
    # >> > outputs = tf.keras.log_result.TimeDistributed(conv_2d_layer)(inputs)

    td_model = Model(td_input, td)

    return td_model, td_input


def handle_lstm_cnn(df, n_lag, n_epochs, n_outputs, test_size):
    start_ts = time.time()
    images = df['Image'].to_numpy()
    images = np.array(list(images), dtype=np.float64)
    images = images / 255.

    df.info()

    print(images[0].shape)

    (df_continious, df_categorical, df_categorical2, scaler) = preprocess(df)

    n_features = df_continious.shape[1]

    n_category = 2  # number of categorical features
    print(f'n_category: {n_category}')
    print(f'n_features: {n_features}')

    categorical_length = len(df_categorical)
    categorical_dim = len(np.unique(df_categorical))

    categorical2_length = len(df_categorical2)
    categorical2_dim = len(np.unique(df_categorical2))

    cat_size = [categorical_length, categorical2_length]  # number of categories in each categorical feature
    cat_embd_dim = [categorical_dim, categorical2_dim]  # embedding dimension for each categorical feature

    numerical_input = Input(shape=(n_lag, n_features), name='continious_input')
    cat_inputs = []
    for i in range(n_category):
        cat_inputs.append(Input(shape=(n_lag, 1), name='categorical' + str(i + 1) + '_input'))

    cat_embedded = []
    for i in range(n_category):
        embed = Embedding(cat_size[i], cat_embd_dim[i])(cat_inputs[i])
        cat_embedded.append(embed)

    time2vec_input = Input(shape=(n_lag, 1), name='time2vec')
    weather2vec_input = Input(shape=(n_lag, 1), name='weather2vec')

    (model_cnn, model_cnn_input) = create_cnn(n_lag, 100, 300, 3)

    print(model_cnn.outputs)
    print(model_cnn.inputs)

    # model_cnn = Reshape((n_lag, -1))(model_cnn)

    t = Time2Vec(n_lag)(time2vec_input)
    w = Weather2Vec(n_lag)(weather2vec_input)

    cat_merged = concatenate(cat_embedded)
    cat_merged = Reshape((n_lag, -1))(cat_merged)
    merged = concatenate([numerical_input, cat_merged, t, w] + model_cnn.outputs)

    x = lstm.create_lstm(merged, n_outputs)

    model = Model([numerical_input] + cat_inputs + [time2vec_input] + [weather2vec_input] + [model_cnn_input], x)
    model.summary()

    print("-> Constructing train/test split")

    print("-> Preprocessing")
    data = df_continious

    n_rows = data.shape[0]

    # Splitting into train and test sets
    train = data[0:int(n_rows * (1 - test_size))]
    test = data[int(n_rows * (1 - test_size)):]

    train_cat = df_categorical[0:int(n_rows * (1 - test_size))]
    test_cat = df_categorical[int(n_rows * (1 - test_size)):]

    train_cat2 = df_categorical2[0:int(n_rows * (1 - test_size))]
    test_cat2 = df_categorical2[int(n_rows * (1 - test_size)):]

    # train_images = images[0:int(n_rows * (1 - test_size))]
    # test_images = images[int(n_rows * (1 - test_size)):]

    # Creating the final scaled frame
    data_s = pd.concat([pd.DataFrame(train), pd.DataFrame(test)])
    data_cat = pd.concat([pd.DataFrame(train_cat), pd.DataFrame(test_cat)])
    data_cat2 = pd.concat([pd.DataFrame(train_cat2), pd.DataFrame(test_cat2)])
    data_images = images

    timestamps = df['Datetime'].to_numpy()

    x, y = utils.timeseries_to_supervised(data_s.values, timestamps, lag=n_lag, n_ahead=n_outputs, target_index=0)
    x_cat, y_cat = utils.timeseries_to_supervised(data_cat.values, timestamps, lag=n_lag, n_ahead=n_outputs,
                                                  target_index=0)
    x_cat2, y_cat2 = utils.timeseries_to_supervised(data_cat2.values, timestamps, lag=n_lag, n_ahead=n_outputs,
                                                    target_index=0)
    x_hour, y_hour = utils.timeseries_to_supervised(data_s.values[:, 2].reshape(-1, 1), timestamps, lag=n_lag,
                                                    n_ahead=n_outputs,
                                                    target_index=0)

    print(f'data_s: {data_s.shape}')
    print(f'data_s_2: {data_s.values[:, 1].reshape(-1, 1).shape}')
    x_weather, y_weather = utils.timeseries_to_supervised(data_s.values[:, 1].reshape(-1, 1), timestamps, lag=n_lag,
                                                          n_ahead=n_outputs,
                                                          target_index=0)

    data_images = np.array([idx for idx, x in enumerate(data_images)]).reshape(-1, 1)

    x_images, y_images = utils.timeseries_to_supervised(data_images, timestamps, lag=n_lag, n_ahead=n_outputs,
                                                        target_index=0)

    print(f'x: {x.shape}, y: {y.shape}')

    train_x, train_y = x[0:int(x.shape[0] * (1 - test_size))], y[0:int(x.shape[0] * (1 - test_size))]
    test_x, test_y = x[int(x.shape[0] * (1 - test_size)):], y[int(x.shape[0] * (1 - test_size)):]

    cat_train_x, cat_train_y = x_cat[0:int(x_cat.shape[0] * (1 - test_size))], y_cat[
                                                                               0:int(x_cat.shape[0] * (1 - test_size))]
    cat_test_x, cat_test_y = x_cat[int(x_cat.shape[0] * (1 - test_size)):], y_cat[
                                                                            int(x_cat.shape[0] * (1 - test_size)):]

    cat2_train_x, cat2_train_y = x_cat2[0:int(x_cat2.shape[0] * (1 - test_size))], y_cat2[
                                                                                   0:int(x_cat2.shape[0] * (
                                                                                               1 - test_size))]
    cat2_test_x, cat2_test_y = x_cat2[int(x_cat2.shape[0] * (1 - test_size)):], y_cat2[
                                                                                int(x.shape[0] * (1 - test_size)):]
    hour_train_x, hour_train_y = x_hour[0:int(x_hour.shape[0] * (1 - test_size))], y_hour[
                                                                                   0:int(x_hour.shape[0] * (
                                                                                           1 - test_size))]
    hour_test_x, hour_test_y = x_hour[int(x_hour.shape[0] * (1 - test_size)):], y_hour[
                                                                                int(x_hour.shape[0] * (1 - test_size)):]

    weather_train_x, weather_train_y = x_weather[0:int(x_weather.shape[0] * (1 - test_size))], y_weather[
                                                                                               0:int(x_weather.shape[
                                                                                                         0] * (
                                                                                                             1 - test_size))]
    weather_test_x, weather_test_y = x_weather[int(x_weather.shape[0] * (1 - test_size)):], y_weather[
                                                                                            int(x_weather.shape[0] * (
                                                                                                    1 - test_size)):]

    images_train_x, images_train_y = x_images[0:int(x_images.shape[0] * (1 - test_size))], y_images[
                                                                                           0:int(x_images.shape[0] * (
                                                                                                   1 - test_size))]
    images_test_x, images_test_y = x_images[int(x_images.shape[0] * (1 - test_size)):], y_images[int(
        x_images.shape[0] * (1 - test_size)):]

    print(f"Train on {len(images_train_x)}")
    print(f"Test on {len(images_test_x)}")

    def generator(total_batches, batch_size):
        idx = 0

        while True:
            start = idx * batch_size
            end = start + batch_size
            print(
                f'\nidx:{idx} yielding {start}:{end} for images {np.squeeze(images[images_train_x[start:end]]).shape}')
            print(
                f'\nidx:{idx} yielding {start}:{end} for images alt: {np.squeeze(images[images_train_x[start:end]], 2).shape}')

            yield {'continious_input': train_x[start:end],
                   'categorical1_input': cat_train_x[start:end],
                   'categorical2_input': cat2_train_x[start:end],
                   'time2vec': hour_train_x[start:end],
                   'weather2vec': weather_train_x[start:end],
                   'images': np.squeeze(images[images_train_x[start:end]], 2)}, train_y[
                                                                                 start:end]
            if idx < total_batches - 1:
                idx += 1
            else:
                idx = 0

    def test_generator(total_batches, batch_size):
        idx = 0

        while True:
            start = idx * batch_size
            end = start + batch_size

            print(
                f'\nidx:{idx} yielding test {start}:{end} for images {np.squeeze(images[images_test_x[start:end]]).shape}')
            print(
                f'\nidx:{idx} yielding test {start}:{end} for images alt: {np.squeeze(images[images_test_x[start:end]], 2).shape}')

            yield {'continious_input': test_x[start:end],
                   'categorical1_input': cat_test_x[start:end],
                   'categorical2_input': cat2_test_x[start:end],
                   'time2vec': hour_test_x[start:end],
                   'weather2vec': weather_test_x[start:end],
                   'images': np.squeeze(images[images_test_x[start:end]], 2)}, test_y[start:end]
            if idx < total_batches - 1:
                idx += 1
            else:
                idx = 0

    print(f'Train: x: {train_x.shape}, y: {train_y.shape}')
    print(f'Test: x: {test_x.shape}, y: {test_y.shape}')

    print(f'CAT Train: x: {cat_train_x.shape}, y: {cat_train_y.shape}')
    print(f'CAT Test: x: {cat_test_x.shape}, y: {cat_test_y.shape}')

    print(f'CAT2 Train: x: {cat2_train_x.shape}, y: {cat2_train_y.shape}')
    print(f'CAT2 Test: x: {cat2_test_x.shape}, y: {cat2_test_y.shape}')

    print(f'Hour Train: x: {hour_train_x.shape}, y: {hour_train_y.shape}')
    print(f'Hour Test: x: {hour_test_x.shape}, y: {hour_test_y.shape}')

    print(f'Weather Train: x: {weather_train_x.shape}, y: {weather_train_y.shape}')
    print(f'Weather Test: x: {weather_test_x.shape}, y: {weather_test_y.shape}')

    print(f'Images Train: x: {images_train_x.shape}, y: {images_train_y.shape}')
    print(f'Images Test: x: {images_test_x.shape}, y: {images_test_y.shape}')

    model.compile(optimizer='adam', loss='mse', metrics=['mae'])

    # train the model

    batch_size = 10
    steps_per_epoch = np.ceil(train_x.shape[0] / batch_size)
    print(f"-> Training on {train_x.shape[0]} ({steps_per_epoch} batches)")

    history = model.fit(generator(steps_per_epoch, batch_size), verbose=1, epochs=n_epochs,
                        steps_per_epoch=steps_per_epoch)

    print("-> Predicting noise levels")
    steps_per_epoch_test = np.ceil(test_x.shape[0] / batch_size)
    print(f"-> Testing on {test_x.shape[0]} ({steps_per_epoch_test} batches)")

    predictions = model.predict(test_generator(steps_per_epoch_test, batch_size), steps=steps_per_epoch_test)
    print(f'Predictions shape: {predictions.shape}')

    predictions_y = np.array([p[n_outputs - 1] for p in predictions])

    print("Predictions:")
    print(predictions_y[0:3])

    scaled_predictions_y = scaler.inverse_transform(predictions_y.reshape(-1, 1))
    print(f'Reshaped predictions shape: {scaled_predictions_y.shape}')
    scaled_test_y = scaler.inverse_transform(test_y.reshape(-1, 1))
    print(f'Reshaped true shape: {scaled_test_y.shape}')

    print(f"True_scaled: {scaled_test_y.shape}")
    print(scaled_test_y[0:3])
    print(f"Predictions_scaled: {scaled_predictions_y.shape}")
    print(scaled_predictions_y[0:3])

    end_ts = time.time()
    utils.log_result('lstm_cnn', history, n_lag, n_epochs, n_outputs,
                     scaled_test_y,
                     scaled_predictions_y, model, end_ts - start_ts)

    return scaled_predictions_y, scaled_test_y, (end_ts - start_ts)


def preprocess(df):
    target = ['TN_Noise_Level_DB']

    continuous = ['WT_Temperature_C', 'Hour', 'WT_Air_Humidity_Percent', 'WT_Road_Temperature_C',
                  'IW_Temp_C', 'IW_Wind_Temp_C',
                  'IW_Air_Pressure_mmHg', 'IW_Air_Humidity_Percent', 'IW_Wind_Speed_MH',
                  'IW_Rain_MMH', 'TT_P1_Speed_Diff_Kmh', 'TT_P1_Travel_Time_Diff_Sec',
                  'TT_P2_Speed_Diff_Kmh', 'TT_P2_Travel_Time_Diff_Sec']

    target_scaler = MinMaxScaler(feature_range=(0, 1))
    target = target_scaler.fit_transform(df[target])

    scaler = MinMaxScaler(feature_range=(0, 1))
    continuous = scaler.fit_transform(df[continuous])

    # categorical = ['TT_P1_Road_Type_Class']
    #
    # # one-hot encoding, all output features are now in the range [0, 1])
    # weather_binarizer = LabelEncoder().fit(df[categorical])
    # categorical = weather_binarizer.transform(df[categorical])

    categorical2 = ['IW_Weather_Cloudiness_Class']

    # one-hot encoding, all output features are now in the range [0, 1])
    weather_binarizer = LabelEncoder().fit(df[categorical2])
    categorical2 = weather_binarizer.transform(df[categorical2])

    categorical3 = ['IW_Weather_Coldness_Class']

    # one-hot encoding, all output features are now in the range [0, 1])
    weather_binarizer = LabelEncoder().fit(df[categorical3])
    categorical3 = weather_binarizer.transform(df[categorical3])

    cont = np.hstack([target, continuous])
    cat = np.hstack([categorical2])
    cat2 = np.hstack([categorical3])

    return cont, cat, cat2, target_scaler
