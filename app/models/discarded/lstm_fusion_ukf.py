import numpy as np
import pandas as pd
from sklearn.metrics import mean_squared_error
from sklearn.preprocessing import LabelEncoder
from sklearn.preprocessing import MinMaxScaler
from tensorflow.keras import Model
from tensorflow.keras.layers import Dense
from tensorflow.keras.layers import Embedding
from tensorflow.keras.layers import Input
from tensorflow.keras.layers import LSTM, Conv1D, MaxPooling1D
from tensorflow.keras.layers import concatenate, Reshape
from pykalman import UnscentedKalmanFilter

import utils
from models import lstm
from models.layers.Time2Vec import Time2Vec
from models.layers.Weather2Vec import Weather2Vec


def handle_lstm_ukf(scaler, df, n_lag, n_epochs, n_outputs, test_size):
    df.info()

    (df_continious, df_continious_raw, df_categorical, df_categorical2) = preprocess(df)

    n_features = df_continious.shape[1]

    n_category = 2  # number of categorical features
    print(f'n_category: {n_category}')
    print(f'n_features: {n_features}')

    categorical_length = len(df_categorical)
    categorical_dim = len(np.unique(df_categorical))

    categorical2_length = len(df_categorical2)
    categorical2_dim = len(np.unique(df_categorical2))

    cat_size = [categorical_length, categorical2_length]  # number of categories in each categorical feature
    cat_embd_dim = [categorical_dim, categorical2_dim]  # embedding dimension for each categorical feature

    numerical_input = Input(shape=(n_lag, n_features), name='continious_input')
    cat_inputs = []
    for i in range(n_category):
        cat_inputs.append(Input(shape=(n_lag, 1), name='categorical' + str(i + 1) + '_input'))

    cat_embedded = []
    for i in range(n_category):
        embed = Embedding(cat_size[i], cat_embd_dim[i])(cat_inputs[i])
        cat_embedded.append(embed)

    cat_merged = concatenate(cat_embedded)
    cat_merged = Reshape((n_lag, -1))(cat_merged)
    merged = concatenate([numerical_input, cat_merged])

    x = lstm.create_lstm(merged, n_outputs)

    model = Model([numerical_input] + cat_inputs, x)
    model.summary()

    print("-> Constructing train/test split")

    print("-> Preprocessing")
    data_raw = df_continious_raw
    data = df_continious

    n_rows = data.shape[0]

    # Splitting into train and test sets
    train_raw = data_raw[0:int(n_rows * (1 - test_size))]
    test_raw = data_raw[int(n_rows * (1 - test_size)):]


    print("test raw @ukf")
    print(test_raw)

    train = data[0:int(n_rows * (1 - test_size))]
    test = data[int(n_rows * (1 - test_size)):]

    train_cat = df_categorical[0:int(n_rows * (1 - test_size))]
    test_cat = df_categorical[int(n_rows * (1 - test_size)):]

    train_cat2 = df_categorical2[0:int(n_rows * (1 - test_size))]
    test_cat2 = df_categorical2[int(n_rows * (1 - test_size)):]

    # Creating the final scaled frame
    data_s = pd.concat([pd.DataFrame(train), pd.DataFrame(test)])
    data_s_raw = pd.concat([pd.DataFrame(train_raw), pd.DataFrame(test_raw)])
    data_cat = pd.concat([pd.DataFrame(train_cat), pd.DataFrame(test_cat)])
    data_cat2 = pd.concat([pd.DataFrame(train_cat2), pd.DataFrame(test_cat2)])

    timestamps = df['Datetime'].to_numpy()

    x, y = utils.timeseries_to_supervised(data_s.values, timestamps, lag=n_lag, n_ahead=n_outputs, target_index=0)
    x_raw, y_raw = utils.timeseries_to_supervised(data_s_raw.values, timestamps, lag=n_lag, n_ahead=n_outputs, target_index=0)
    x_cat, y_cat = utils.timeseries_to_supervised(data_cat.values, timestamps, lag=n_lag, n_ahead=n_outputs,
                                                  target_index=0)

    x_cat2, y_cat2 = utils.timeseries_to_supervised(data_cat2.values, timestamps, lag=n_lag, n_ahead=n_outputs,
                                                    target_index=0)


    print(f'x: {x.shape}, y: {y.shape}')

    train_x, train_y = x[0:int(x.shape[0] * (1 - test_size))], y[0:int(x.shape[0] * (1 - test_size))]
    train_x_raw, train_y_raw = x_raw[0:int(x_raw.shape[0] * (1 - test_size))], y_raw[0:int(x.shape[0] * (1 - test_size))]
    test_x, test_y = x[int(x.shape[0] * (1 - test_size)):], y[int(x.shape[0] * (1 - test_size)):]
    test_x_raw, test_y_raw = x_raw[int(x_raw.shape[0] * (1 - test_size)):], y_raw[int(x_raw.shape[0] * (1 - test_size)):]

    print("test_y @ukf")
    print(test_y_raw)

    cat_train_x, cat_train_y = x_cat[0:int(x_cat.shape[0] * (1 - test_size))], y_cat[
                                                                               0:int(x_cat.shape[0] * (1 - test_size))]
    cat_test_x, cat_test_y = x_cat[int(x_cat.shape[0] * (1 - test_size)):], y_cat[int(x.shape[0] * (1 - test_size)):]

    cat2_train_x, cat2_train_y = x_cat2[0:int(x_cat2.shape[0] * (1 - test_size))], y_cat2[
                                                                                   0:int(x_cat2.shape[0] * (
                                                                                               1 - test_size))]
    cat2_test_x, cat2_test_y = x_cat2[int(x_cat2.shape[0] * (1 - test_size)):], y_cat2[
                                                                                int(x.shape[0] * (1 - test_size)):]

    print(f'Train: x: {train_x.shape}, y: {train_y.shape}')
    print(f'Test: x: {test_x.shape}, y: {test_y.shape}')

    print(f'CAT Train: x: {cat_train_x.shape}, y: {cat_train_y.shape}')
    print(f'CAT Test: x: {cat_test_x.shape}, y: {cat_test_y.shape}')

    print(f'CAT2 Train: x: {cat2_train_x.shape}, y: {cat2_train_y.shape}')
    print(f'CAT2 Test: x: {cat2_test_x.shape}, y: {cat2_test_y.shape}')

    model.compile(optimizer='adam', loss='mae', metrics=['mae'])

    # train the model
    print("-> Training")
    history = model.fit(x=[train_x, cat_train_x, cat2_train_x], y=train_y_raw,
                        validation_data=([test_x, cat_test_x, cat2_test_x], test_y_raw),
                        epochs=n_epochs, batch_size=32)

    print("-> Predicting noise levels")
    predictions = model.predict([test_x, cat_test_x, cat2_test_x])
    print(f'Predictions shape: {predictions.shape}')
    predictions_y = np.array([p[n_outputs - 1] for p in predictions])

    print("Predictions:")
    print(predictions_y[0:3])

    scaled_predictions_y = scaler.inverse_transform(predictions_y.reshape(-1, 1))
    print(f'Reshaped predictions shape: {scaled_predictions_y.shape}')
    scaled_test_y = scaler.inverse_transform(test_y_raw.reshape(-1, 1))
    print(f'Reshaped true shape: {scaled_test_y.shape}')

    print(f'RMSE - asdf: {np.sqrt(mean_squared_error(scaled_test_y, scaled_predictions_y))}')

    print("True_scaled:")
    print(scaled_test_y[0:3])
    print("Predictions_scaled:")
    print(scaled_predictions_y[0:3])
    # print((scaled_test_y - scaled_predictions_y)[0:3])

    utils.log_result('lstm_fusion_ukf', history, n_lag, n_epochs, n_outputs,
                     scaled_test_y, scaled_predictions_y, model)

    return scaled_predictions_y, scaled_test_y


def preprocess(df):
    target = ['TN_Noise_Level_DB']

    continuous = ['WT_Temperature_C', 'Hour', 'WT_Air_Humidity_Percent', 'WT_Road_Temperature_C',
                  'IW_Temp_C', 'IW_Wind_Temp_C',
                  'IW_Air_Pressure_mmHg', 'IW_Air_Humidity_Percent', 'IW_Wind_Speed_MH',
                  'IW_Rain_MMH', 'TT_P1_Speed_Diff_Kmh', 'TT_P1_Travel_Time_Diff_Sec',
                  'TT_P2_Speed_Diff_Kmh', 'TT_P2_Travel_Time_Diff_Sec']

    target = df[target]

    scaler = MinMaxScaler(feature_range=(0, 1))
    continuous = scaler.fit_transform(df[continuous])

    # categorical = ['TT_P1_Road_Type_Class']
    #
    # # one-hot encoding, all output features are now in the range [0, 1])
    # weather_binarizer = LabelEncoder().fit(df[categorical])
    # categorical = weather_binarizer.transform(df[categorical])

    categorical2 = ['IW_Weather_Cloudiness_Class']

    # one-hot encoding, all output features are now in the range [0, 1])
    weather_binarizer = LabelEncoder().fit(df[categorical2])
    categorical2 = weather_binarizer.transform(df[categorical2])

    categorical3 = ['IW_Weather_Coldness_Class']

    # one-hot encoding, all output features are now in the range [0, 1])
    weather_binarizer = LabelEncoder().fit(df[categorical3])
    categorical3 = weather_binarizer.transform(df[categorical3])

    cont = np.hstack([target, continuous])
    cont_raw = np.hstack([target, continuous])
    cat = np.hstack([categorical2])
    cat2 = np.hstack([categorical3])

    # TODO 14 = hardcoded
    ukf = UnscentedKalmanFilter(n_dim_obs=15)

    print(f'Shape pre: {cont.shape}')

    cont = ukf.filter(cont[:, :].astype(np.float32))

    cont = np.array(cont[0])

    print(f'Shape post: {cont.shape}')

    return cont, cont_raw, cat, cat2
