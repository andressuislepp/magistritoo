import time

import numpy as np
import pandas as pd
from sklearn.metrics import mean_squared_error
from sklearn.preprocessing import LabelEncoder
from sklearn.preprocessing import MinMaxScaler
from tensorflow.keras import Model
from tensorflow.keras.layers import Dense
from tensorflow.keras.layers import Embedding
from tensorflow.keras.layers import Input
from tensorflow.keras.layers import LSTM, Conv1D, MaxPooling1D
from tensorflow.keras.layers import concatenate, Reshape

import utils
from models import lstm, config
from models.layers.Time2Vec import Time2Vec
from models.layers.Weather2Vec import Weather2Vec


def handle_lstm_ekf_2vec(scaler, df, n_lag, n_epochs, n_outputs, test_size):
    start_ts = time.time()
    df.info()

    (df_continious, df_continious_raw, df_cats) = preprocess(df)

    n_features = df_continious.shape[1]
    n_category = len(df_cats)  # number of categorical features
    print(f'n_category: {n_category}')
    print(f'n_features: {n_features}')

    numerical_input = Input(shape=(n_lag, n_features), name='continious_input')
    cat_inputs = []
    for i in range(n_category):
        cat_inputs.append(Input(shape=(n_lag, 1), name='categorical' + str(i + 1) + '_input'))

    cat_embedded = []
    for i in range(n_category):
        df_cat = df_cats[i]
        df_dim = len(np.unique(df_cat))
        embed = Embedding(len(df_cat), df_dim)(cat_inputs[i])
        cat_embedded.append(embed)

    time2vec_input = Input(shape=(n_lag, 1), name='time2vec')

    t = Time2Vec(n_lag)(time2vec_input)

    cat_merged = concatenate(cat_embedded)
    cat_merged = Reshape((n_lag, -1))(cat_merged)
    merged = concatenate([numerical_input, cat_merged, t])

    x = lstm.create_lstm(merged, n_outputs)

    model = Model([numerical_input] + cat_inputs + [time2vec_input], x)
    model.summary()

    print("-> Constructing train/test split")

    print("-> Preprocessing")
    data_raw = df_continious_raw
    data = df_continious

    n_rows = data.shape[0]

    # Splitting into train and test sets
    train_raw = data_raw[0:int(n_rows * (1 - test_size))]
    test_raw = data_raw[int(n_rows * (1 - test_size)):]


    print("test raw @ukf")
    print(test_raw)

    train = data[0:int(n_rows * (1 - test_size))]
    test = data[int(n_rows * (1 - test_size)):]

    # Creating the final scaled frame
    data_s = pd.concat([pd.DataFrame(train), pd.DataFrame(test)])
    data_s_raw = pd.concat([pd.DataFrame(train_raw), pd.DataFrame(test_raw)])

    data_cats = []

    for i in range(n_category):
        train_cat = df_cats[i][0:int(n_rows * (1 - test_size))]
        test_cat = df_cats[i][int(n_rows * (1 - test_size)):]
        data_cats.append(pd.concat([pd.DataFrame(train_cat), pd.DataFrame(test_cat)]))

    timestamps = df['Datetime'].to_numpy()

    x, y = utils.timeseries_to_supervised(data_s.values, timestamps, lag=n_lag, n_ahead=n_outputs, target_index=0)
    x_raw, y_raw = utils.timeseries_to_supervised(data_s_raw.values, timestamps, lag=n_lag, n_ahead=n_outputs, target_index=0)

    x_cats = []
    y_cats = []

    for i in range(n_category):
        x_cat, y_cat = utils.timeseries_to_supervised(data_cats[i].values, timestamps, lag=n_lag, n_ahead=n_outputs,
                                                      target_index=0)
        x_cats.append(x_cat)
        y_cats.append(y_cat)

    x_hour, y_hour = utils.timeseries_to_supervised(data_s_raw.values[:, config.timeofday_index].reshape(-1, 1), timestamps, lag=n_lag,
                                                    n_ahead=n_outputs,
                                                    target_index=0)

    print(f'x: {x.shape}, y: {y.shape}')

    train_x, train_y = x[0:int(x.shape[0] * (1 - test_size))], y[0:int(x.shape[0] * (1 - test_size))]
    train_x_raw, train_y_raw = x_raw[0:int(x_raw.shape[0] * (1 - test_size))], y_raw[0:int(x.shape[0] * (1 - test_size))]
    test_x, test_y = x[int(x.shape[0] * (1 - test_size)):], y[int(x.shape[0] * (1 - test_size)):]
    test_x_raw, test_y_raw = x_raw[int(x_raw.shape[0] * (1 - test_size)):], y_raw[int(x_raw.shape[0] * (1 - test_size)):]

    print("test_y @ukf")
    print(test_y_raw)

    cats_train_x = []
    cats_train_y = []
    cats_test_x = []
    cats_test_y = []

    for i in range(n_category):
        cat_train_x, cat_train_y = x_cats[i][0:int(x_cats[i].shape[0] * (1 - test_size))], y_cats[i][
                                                                                           0:int(x_cats[i].shape[0] * (
                                                                                                   1 - test_size))]
        cat_test_x, cat_test_y = x_cats[i][int(x_cats[i].shape[0] * (1 - test_size)):], y_cats[i][
                                                                                        int(x.shape[0] * (
                                                                                                    1 - test_size)):]

        cats_train_x.append(cat_train_x)
        cats_train_y.append(cat_train_y)
        cats_test_x.append(cat_test_x)
        cats_test_y.append(cat_test_y)

    hour_train_x, hour_train_y = x_hour[0:int(x_hour.shape[0] * (1 - test_size))], y_hour[
                                                                                   0:int(x_hour.shape[0] * (
                                                                                               1 - test_size))]
    hour_test_x, hour_test_y = x_hour[int(x_hour.shape[0] * (1 - test_size)):], y_hour[
                                                                                int(x.shape[0] * (1 - test_size)):]

    print(f'Train: x: {train_x.shape}, y: {train_y.shape}')
    print(f'Test: x: {test_x.shape}, y: {test_y.shape}')

    for i in range(n_category):
        print(f'CAT_{i} Train: x: {cats_train_x[i].shape}, y: {cats_train_y[i].shape}')
        print(f'CAT_{i} Test: x: {cats_test_x[i].shape}, y: {cats_test_y[i].shape}')

    print(f'Hour Train: x: {hour_train_x.shape}, y: {hour_train_y.shape}')
    print(f'Hour Test: x: {hour_test_x.shape}, y: {hour_test_y.shape}')

    model.compile(optimizer='adam', loss='mae', metrics=['mae'])

    # train the model
    print("-> Training")
    history = model.fit(x=[train_x] + cats_train_x + [hour_train_x], y=train_y_raw,
                        validation_data=([test_x] + cats_test_x + [hour_test_x], test_y_raw),
                        epochs=n_epochs, batch_size=32)

    print("-> Predicting noise levels")
    # Prediction for train set
    predictions_train = model.predict([train_x] + cats_train_x + [hour_train_x])
    predictions_train_y = np.array([p[n_outputs - 1] for p in predictions_train])
    scaled_predictions_train_y = scaler.inverse_transform(predictions_train_y.reshape(-1, 1))
    scaled_train_y = scaler.inverse_transform(train_y.reshape(-1, 1))

    # Prediction for test set
    predictions = model.predict([test_x] + cats_test_x + [hour_test_x])
    print(f'Predictions shape: {predictions.shape}')
    predictions_y = np.array([p[n_outputs - 1] for p in predictions])

    print("Predictions:")
    print(predictions_y[0:3])

    scaled_predictions_y = scaler.inverse_transform(predictions_y.reshape(-1, 1))
    print(f'Reshaped predictions shape: {scaled_predictions_y.shape}')
    scaled_test_y = scaler.inverse_transform(test_y_raw.reshape(-1, 1))
    print(f'Reshaped true shape: {scaled_test_y.shape}')

    print(f'RMSE - asdf: {np.sqrt(mean_squared_error(scaled_test_y, scaled_predictions_y))}')

    print("True_scaled:")
    print(scaled_test_y[0:3])
    print("Predictions_scaled:")
    print(scaled_predictions_y[0:3])
    # print((scaled_test_y - scaled_predictions_y)[0:3])

    end_ts = time.time()

    utils.log_result('lstm_fusion_ukf_2vec', history, n_lag, n_epochs, n_outputs,
                     scaled_test_y, scaled_predictions_y, model, end_ts - start_ts)

    return scaled_predictions_y, scaled_test_y, scaled_predictions_train_y, scaled_train_y, (end_ts - start_ts)


def preprocess(df):
    target = config.target_variable

    continuous = config.continuous_variables

    target = df[target]

    scaler = MinMaxScaler(feature_range=(0, 1))
    continuous = scaler.fit_transform(df[continuous])

    cats = []
    categorical = config.categorical_variables

    for c in categorical:
        binarizer = LabelEncoder().fit(df[c])
        cat = binarizer.transform(df[c])
        cats.append(np.hstack([cat]))

    cont = np.hstack([target, continuous])

    cont_raw = np.hstack([target, continuous])

    ukf = UnscentedKalmanFilter(n_dim_obs=len(config.continuous_variables) + 1, initial_state_mean=10)

    print(f'Shape pre: {cont.shape}')

    cont = ukf.filter(cont[:, :].astype(np.float32))

    cont = np.array(cont[0])

    print(f'Shape post: {cont.shape}')

    return cont, cont_raw, cats
