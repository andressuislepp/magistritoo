import time

import numpy as np
import pandas as pd
from sklearn.metrics import mean_squared_error
from sklearn.preprocessing import LabelEncoder
from sklearn.preprocessing import MinMaxScaler
from tensorflow.keras import Input
from tensorflow.keras import Model
from tensorflow.keras.layers import Conv2D
from tensorflow.keras.layers import Dense
from tensorflow.keras.layers import Embedding
from tensorflow.keras.layers import Flatten
from tensorflow.keras.layers import LSTM
from tensorflow.keras.layers import MaxPool2D, MaxPooling1D, Conv1D
from tensorflow.keras.layers import TimeDistributed
from tensorflow.keras.layers import concatenate, Reshape
from tensorflow.keras.models import Sequential
from tensorflow.keras.layers import Activation
from tensorflow.keras.layers import BatchNormalization
from tensorflow.keras.layers import Conv2D
from tensorflow.keras.layers import Dense
from tensorflow.keras.layers import Dropout
from tensorflow.keras.layers import Embedding
from tensorflow.keras.layers import Flatten
from tensorflow.keras.layers import MaxPooling2D
from tensorflow.keras.layers import TimeDistributed
from tensorflow.keras.layers import concatenate, Reshape

import utils
from models import lstm, config
from models.layers.Time2Vec import Time2Vec
from models.layers.Weather2Vec import Weather2Vec


def create_cnn(n_lag, width, height, depth, filters=(100, 200, 300)):
    input_shape = (height, width, depth)
    channel_dim = -1

    inputs = Input(shape=input_shape)

    for (i, f) in enumerate(filters):
        if i == 0:
            x = inputs

        # CONV => RELU > BN => POOL
        x = Conv2D(f, (3, 3), padding="same")(x)
        x = Activation("relu")(x)
        x = BatchNormalization(axis=channel_dim)(x)
        x = MaxPooling2D(pool_size=(2, 2))(x)
    x = Flatten()(x)
    x = Dense(1024, activation="relu")(x)
    x = BatchNormalization(axis=channel_dim)(x)
    x = Dropout(0.2)(x)

    # x = Dense(1, activation="linear")(x)

    model = Model(inputs, x)

    td_input = Input(shape=(n_lag, height, width, depth), name="images")

    td = TimeDistributed(model)(td_input)

    # inputs = tf.keras.Input(shape=(10, 128, 128, 3))
    # >> > conv_2d_layer = tf.keras.layers.Conv2D(64, (3, 3))
    # >> > outputs = tf.keras.log_result.TimeDistributed(conv_2d_layer)(inputs)

    td_model = Model(td_input, td)

    return td_model, td_input


def handle_lstm_img_cnn(scaler, df, n_lag, n_epochs, n_outputs, test_size):
    start_ts = time.time()

    images = df['Image'].to_numpy()
    images = np.array(list(images), dtype=np.float64)
    images = images / 255.

    df.info()

    print(images[0].shape)
    (df_continious, df_cats) = preprocess(df)

    (model_cnn, model_cnn_input) = create_cnn(n_lag, 100, 300, 3)

    merged = concatenate(model_cnn.outputs)

    x = lstm.create_lstm(merged, n_outputs)

    # x = lstm.create_lstm(model_cnn.outputs, n_outputs)

    model = Model(model_cnn_input, x)
    model.summary()

    print("-> Constructing train/test split")

    print("-> Preprocessing")
    data = df_continious

    n_rows = data.shape[0]

    # Splitting into train and test sets
    train = data[0:int(n_rows * (1 - test_size))]
    test = data[int(n_rows * (1 - test_size)):]

    # Creating the final scaled frame
    data_s = pd.concat([pd.DataFrame(train), pd.DataFrame(test)])
    data_images = images

    timestamps = df['Datetime'].to_numpy()

    x, y = utils.timeseries_to_supervised(data_s.values, timestamps, lag=n_lag, n_ahead=n_outputs, target_index=0)



    data_images = np.array([idx for idx, x in enumerate(data_images)]).reshape(-1, 1)

    x_images, y_images = utils.timeseries_to_supervised(data_images, timestamps, lag=n_lag, n_ahead=n_outputs,
                                                        target_index=0)

    print(f'x: {x.shape}, y: {y.shape}')

    train_x, train_y = x[0:int(x.shape[0] * (1 - test_size))], y[0:int(x.shape[0] * (1 - test_size))]
    test_x, test_y = x[int(x.shape[0] * (1 - test_size)):], y[int(x.shape[0] * (1 - test_size)):]

    print("test_y @img")
    print(test_y)

    images_train_x, images_train_y = x_images[0:int(x_images.shape[0] * (1 - test_size))], y_images[
                                                                                           0:int(x_images.shape[0] * (
                                                                                                   1 - test_size))]
    images_test_x, images_test_y = x_images[int(x_images.shape[0] * (1 - test_size)):], y_images[int(
        x_images.shape[0] * (1 - test_size)):]

    print(f"Train on {len(images_train_x)}")
    print(f"Test on {len(images_test_x)}")

    def generator(total_batches, batch_size):
        idx = 0

        while True:
            start = idx * batch_size
            end = start + batch_size
            print(
                f'\nidx:{idx} yielding {start}:{end} for images {np.squeeze(images[images_train_x[start:end]]).shape}')
            print(
                f'\nidx:{idx} yielding {start}:{end} for images alt: {np.squeeze(images[images_train_x[start:end]], 2).shape}')

            yield {'images': np.squeeze(images[images_train_x[start:end]], 2)}, train_y[
                                                                                 start:end]
            if idx < total_batches - 1:
                idx += 1
            else:
                idx = 0

    def test_generator(total_batches, batch_size):
        idx = 0

        while True:
            start = idx * batch_size
            end = start + batch_size

            print(
                f'\nidx:{idx} yielding test {start}:{end} for images {np.squeeze(images[images_test_x[start:end]]).shape}')
            print(
                f'\nidx:{idx} yielding test {start}:{end} for images alt: {np.squeeze(images[images_test_x[start:end]], 2).shape}')

            yield {'images': np.squeeze(images[images_test_x[start:end]], 2)}, test_y[start:end]
            if idx < total_batches - 1:
                idx += 1
            else:
                idx = 0

    print(f'Train: x: {train_x.shape}, y: {train_y.shape}')
    print(f'Test: x: {test_x.shape}, y: {test_y.shape}')

    print(f'Images Train: x: {images_train_x.shape}, y: {images_train_y.shape}')
    print(f'Images Test: x: {images_test_x.shape}, y: {images_test_y.shape}')

    model.compile(optimizer='adam', loss='mse', metrics=['mae'])

    # train the model

    batch_size = 2
    steps_per_epoch = np.ceil(train_x.shape[0] / batch_size)
    steps_per_epoch_test = np.ceil(test_x.shape[0] / batch_size)

    print(f"-> Training on {train_x.shape[0]} ({steps_per_epoch} batches)")

    history = model.fit(generator(steps_per_epoch, batch_size), verbose=1, epochs=n_epochs,
                        steps_per_epoch=steps_per_epoch)

    print("-> Predicting noise levels")

    # Prediction for train set
    predictions_train = model.predict(generator(steps_per_epoch, batch_size), steps=steps_per_epoch)
    predictions_train_y = np.array([p[n_outputs - 1] for p in predictions_train])
    scaled_predictions_train_y = scaler.inverse_transform(predictions_train_y.reshape(-1, 1))
    scaled_train_y = scaler.inverse_transform(train_y.reshape(-1, 1))

    # Prediction for test set
    print(f"-> Testing on {test_x.shape[0]} ({steps_per_epoch_test} batches)")

    predictions = model.predict(test_generator(steps_per_epoch_test, batch_size), steps=steps_per_epoch_test)
    print(f'Predictions shape: {predictions.shape}')
    predictions_y = np.array([p[n_outputs - 1] for p in predictions])

    print("Predictions:")
    print(predictions_y[0:3])

    scaled_predictions_y = scaler.inverse_transform(predictions_y.reshape(-1, 1))
    print(f'Reshaped predictions shape: {scaled_predictions_y.shape}')
    scaled_test_y = scaler.inverse_transform(test_y.reshape(-1, 1))
    print(f'Reshaped true shape: {scaled_test_y.shape}')

    print(f"True_scaled: {scaled_test_y.shape}")
    print(scaled_test_y[0:3])
    print(f"Predictions_scaled: {scaled_predictions_y.shape}")
    print(scaled_predictions_y[0:3])

    end_ts = time.time()

    utils.log_result('lstm_img_cnn', history, n_lag, n_epochs, n_outputs,
                     scaled_test_y, scaled_predictions_y, model, end_ts - start_ts)

    return scaled_predictions_y, scaled_test_y, scaled_predictions_train_y, scaled_train_y, (end_ts - start_ts)


def preprocess(df):
    target = config.target_variable

    continuous = config.continuous_variables

    target = df[target]

    scaler = MinMaxScaler(feature_range=(0, 1))
    continuous = scaler.fit_transform(df[continuous])

    cats = []
    categorical = config.categorical_variables

    for c in categorical:
        binarizer = LabelEncoder().fit(df[c])
        cat = binarizer.transform(df[c])
        cats.append(np.hstack([cat]))

    cont = np.hstack([target, continuous])

    return cont, cats
