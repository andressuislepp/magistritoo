target_variable = ['TN_Noise_Level_DB']

continuous_variables = ['TT_P1_Speed_Diff_Kmh', 'TT_P2_Speed_Diff_Kmh',
                        'TT_P1_Travel_Time_Diff_Sec', 'TT_P2_Travel_Time_Diff_Sec',
                        'Minute_Of_Day', 'WT_Road_Temperature_C', 'WT_Air_Humidity_Percent',
                        'IW_Temp_C', 'IW_Wind_Temp_C', 'IW_Wind_Speed_MH']

timeofday_index = 5

categorical_variables = ['IW_Wind_Direction_Mark', 'IW_Weather_Cloudiness_Class', 'IW_Weather_Coldness_Class']
