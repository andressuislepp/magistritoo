from tensorflow.keras.layers import LSTM, Conv1D, MaxPooling1D, Dropout, Dense


def create_lstm(model, n_outputs):
    x = Conv1D(filters=64, kernel_size=1, activation='relu')(model)
    x = MaxPooling1D(pool_size=2, padding='same')(x)
    x = Dropout(0.2)(x)
    x = Conv1D(filters=64, kernel_size=1, activation='relu')(x)
    x = MaxPooling1D(pool_size=2, padding='same')(x)
    x = Dropout(0.2)(x)
    x = Conv1D(filters=64, kernel_size=1, activation='relu')(x)
    x = MaxPooling1D(pool_size=2, padding='same')(x)

    x = LSTM(10, activation='relu', return_sequences=False)(x)
    x = Dense(n_outputs)(x)

    return x