import time

import numpy as np
import pandas as pd
from tensorflow.keras import Input, Model
from sklearn.metrics import mean_squared_error
from sklearn.preprocessing import MinMaxScaler
from tensorflow.keras.layers import Dense
from tensorflow.keras.layers import LSTM
from tensorflow.keras.models import Sequential
from tensorflow.keras.layers import LSTM, Conv1D, MaxPooling1D, Dropout, Dense


import utils
from models import lstm


def handle_lstm(scaler, df, n_lag, n_epochs, n_output, test_size):
    start_ts = time.time()
    df.info()

    (df2) = preprocess(df)

    print("-> Constructing train/test split")

    print("-> Preprocessing")
    data = df2

    n_features = data.shape[1]
    n_rows = data.shape[0]

    # Splitting into train and test sets
    train = data[0:int(n_rows * (1 - test_size))]
    test = data[int(n_rows * (1 - test_size)):]

    # Creating the final scaled frame
    data_s = pd.concat([pd.DataFrame(train), pd.DataFrame(test)])

    timestamps = df['Datetime'].to_numpy()
    x, y = utils.timeseries_to_supervised(data_s.values, timestamps, lag=n_lag, n_ahead=n_output, target_index=0)

    train_x, train_y = x[0:int(x.shape[0] * (1 - test_size))], y[0:int(x.shape[0] * (1 - test_size))]
    test_x, test_y = x[int(x.shape[0] * (1 - test_size)):], y[int(x.shape[0] * (1 - test_size)):]

    print(f'Train: x: {train_x.shape}, y: {train_y.shape}')
    print(f'Test: x: {test_x.shape}, y: {test_y.shape}')


    input = Input(shape=(n_lag, n_features), name='input')
    x = lstm.create_lstm(input, n_output)

    model = Model(input, x)
    model.compile(optimizer='adam', loss='mae')

    # train the model
    print("-> Training")
    history = model.fit(x=train_x, y=train_y,
                        validation_data=(test_x, test_y),
                        epochs=n_epochs, batch_size=36)

    print("-> Predicting noise levels")
    # Prediction for train set
    predictions_train = model.predict(train_x)
    predictions_train_y = np.array([p[n_output - 1] for p in predictions_train])
    scaled_predictions_train_y = scaler.inverse_transform(predictions_train_y.reshape(-1, 1))
    scaled_train_y = scaler.inverse_transform(train_y.reshape(-1, 1))

    # Prediction for test set
    predictions = model.predict(test_x)
    print(f'Predictions shape: {predictions.shape}')

    predictions_y = np.array([p[n_output-1] for p in predictions])

    print("Predictions:")
    print(predictions_y[0:3])

    scaled_predictions_y = scaler.inverse_transform(predictions_y.reshape(-1, 1))
    print(f'Reshaped predictions shape: {scaled_predictions_y.shape}')
    scaled_test_y = scaler.inverse_transform(test_y.reshape(-1, 1))
    print(f'Reshaped true shape: {scaled_test_y.shape}')

    print(f'RMSE - asdf: {np.sqrt(mean_squared_error(scaled_test_y, scaled_predictions_y))}')

    print("True_scaled:")
    print(scaled_test_y[0:3])
    print("Predictions_scaled:")
    print(scaled_predictions_y[0:3])
    # print((scaled_test_y - scaled_predictions_y)[0:3])

    end_ts = time.time()
    utils.log_result('lstm_univariate', history, n_lag, n_epochs, n_output,
                     scaled_test_y, scaled_predictions_y, model, end_ts - start_ts)

    return scaled_predictions_y, scaled_test_y, scaled_predictions_train_y, scaled_train_y, (end_ts - start_ts)


def preprocess(df):
    target = ['TN_Noise_Level_DB']

    target = df[target]

    res = np.hstack([target])

    return res
