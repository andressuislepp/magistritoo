import time

import numpy as np

import utils
from models import lstm_img_cnn, lstm_fusion_ukf_2vec, lstm_nofusion, lstm_fusion_skf_2vec, lstm_fusion_kf_2vec, \
    lstm_univariate, lstm_cnn_skf
from pykalman import UnscentedKalmanFilter, KalmanFilter
from sklearn.svm import SVR

from models.timeseries import arima, linear_regression, moving_average, naive


def handle_decision_fusion(ff_name, img_pred, y_pred, y_true, n_lag, n_epochs, n_outputs, img_x_pred, x_pred, x_true, time_elapsed):
    merged_train_data = np.zeros(shape=(len(x_pred), 2))

    merged_train_data[:, 0] = img_x_pred[:, 0]
    merged_train_data[:, 1] = x_pred[:, 0]

    merged_data = np.zeros(shape=(len(y_pred), 2))

    merged_data[:, 0] = img_pred[:, 0]
    merged_data[:, 1] = y_pred[:, 0]

    # Apply Decision fusion
    # AVG
    avg_start_ts = time.time()
    avg_df_pred = (y_pred + img_pred) / 2
    avg_time = time.time() - avg_start_ts
    # DOT

    # KF
    kf_start_ts = time.time()
    kf = KalmanFilter(initial_state_mean=59, n_dim_obs=2)
    df_kf_pred = kf.filter(merged_data[:, :].astype(np.float32))
    df_kf_pred = np.array(df_kf_pred[0])
    kf_time = time.time() - kf_start_ts
    print("KF Decision fusion done")

    # Smoothed KF
    skf_start_ts = time.time()
    kf = KalmanFilter(initial_state_mean=59, n_dim_obs=2)
    df_skf_pred = kf.em(merged_data[:, :].astype(np.float32)).smooth(merged_data[:, :].astype(np.float32))
    df_skf_pred = np.array(df_skf_pred[0])
    skf_time = time.time() - skf_start_ts
    print("SKF Decision fusion done")

    # Uncented KF
    ukf_start_ts = time.time()
    ukf = UnscentedKalmanFilter(n_dim_obs=2, initial_state_mean=59)
    df_ukf_pred = ukf.filter(merged_data[:, :].astype(np.float32))
    df_ukf_pred = np.array(df_ukf_pred[0])
    ukf_time = time.time() - ukf_start_ts
    print("UKF Decision fusion")
    print(df_ukf_pred)

    # SVM
    svm_start_ts = time.time()
    regressor = SVR(kernel='rbf')
    regressor.fit(merged_train_data[:, :], x_true)
    df_svm_pred = regressor.predict(merged_data[:, :].astype(np.float32))
    svm_time = time.time() - svm_start_ts
    print(df_svm_pred)

    utils.log_result(f'{ff_name}_avg_df', None, n_lag, n_epochs, n_outputs, y_true, avg_df_pred, None, avg_time + time_elapsed)
    utils.log_result(f'{ff_name}_kf_df', None, n_lag, n_epochs, n_outputs, y_true, df_kf_pred, None, kf_time + time_elapsed)
    utils.log_result(f'{ff_name}_skf_df', None, n_lag, n_epochs, n_outputs, y_true, df_skf_pred, None, skf_time + time_elapsed)
    utils.log_result(f'{ff_name}_ukf_df', None, n_lag, n_epochs, n_outputs, y_true, df_ukf_pred, None, ukf_time + time_elapsed)
    utils.log_result(f'{ff_name}_svm_df', None, n_lag, n_epochs, n_outputs, y_true, df_svm_pred, None, svm_time + time_elapsed)


def handle_df_full(target_scaler, df, n_lag, n_epochs, n_outputs, test_size):
    # lstm_cnn_skf.handle_lstm_cnn_skf(
    #     target_scaler, df, n_lag, n_epochs, n_outputs,
    #     test_size)
    # (cnn_pred, cnn_test, cnn_train_pred, cnn_train_true, cnn_train_time) = lstm_img_cnn.handle_lstm_img_cnn(target_scaler, df, n_lag, n_epochs, n_outputs,
    #                                                             test_size)
    (skf_pred, skf_test, skf_train_pred, skf_train_true, skf_train_time) = lstm_fusion_skf_2vec.handle_lstm_skf_2vec(target_scaler, df, n_lag, n_epochs, n_outputs, test_size)
    (uni_pred, uni_test, uni_train_pred, uni_train_true, uni_train_time) = lstm_univariate.handle_lstm(target_scaler, df, n_lag, n_epochs, n_outputs, test_size)
    (kf_pred, kf_test, kf_train_pred, kf_train_true, kf_train_time) = lstm_fusion_kf_2vec.handle_lstm_kf_2vec(target_scaler, df, n_lag, n_epochs, n_outputs, test_size)
    (ukf_pred, ukf_test, ukf_train_pred, ukf_train_true, ukf_train_time) = lstm_fusion_ukf_2vec.handle_lstm_ukf_2vec(target_scaler, df, n_lag, n_epochs, n_outputs, test_size)
    (none_pred, none_test, none_train_pred, none_train_true, none_train_time) = lstm_nofusion.handle_lstm_nofusion(target_scaler, df, n_lag, n_epochs, n_outputs,
                                                                     test_size)

    naive.handle_naive(target_scaler, df, n_lag, n_outputs, test_size)
    moving_average.handle_moving_average(target_scaler, df, n_lag, n_outputs, test_size)
    linear_regression.handle_linear_regression(target_scaler, df, n_lag, n_outputs, test_size)
    arima.handle_arima(target_scaler, df, n_lag, n_outputs, test_size)

    # handle_decision_fusion('uni_cnn', cnn_pred, uni_pred, cnn_test, n_lag, n_epochs, n_outputs, cnn_train_pred, uni_train_pred, cnn_train_true, uni_train_time + cnn_train_time)
    # handle_decision_fusion('none_cnn', cnn_pred, none_pred, cnn_test, n_lag, n_epochs, n_outputs, cnn_train_pred, none_train_pred, none_train_true, none_train_time + cnn_train_time)
    # handle_decision_fusion('skf_cnn', cnn_pred, skf_pred, cnn_test, n_lag, n_epochs, n_outputs, cnn_train_pred, skf_train_pred, cnn_train_true, skf_train_time + cnn_train_time)
    # handle_decision_fusion('ukf_cnn', cnn_pred, ukf_pred, cnn_test, n_lag, n_epochs, n_outputs, cnn_train_pred, ukf_train_pred, cnn_train_true, ukf_train_time + cnn_train_time)
    # handle_decision_fusion('kf_cnn', cnn_pred, kf_pred, cnn_test, n_lag, n_epochs, n_outputs, cnn_train_pred, kf_train_pred, cnn_train_true, kf_train_time + cnn_train_time)
