import sys
import time

import numpy as np
import pandas as pd
from sklearn.metrics import mean_squared_error
from sklearn.preprocessing import LabelEncoder
from sklearn.preprocessing import MinMaxScaler
from tensorflow.keras import Input
from tensorflow.keras import Model
from tensorflow.keras.layers import Activation
from tensorflow.keras.layers import BatchNormalization
from tensorflow.keras.layers import Conv2D
from tensorflow.keras.layers import Dense
from tensorflow.keras.layers import Dropout
from tensorflow.keras.layers import Embedding
from tensorflow.keras.layers import Flatten
from tensorflow.keras.layers import LSTM
from tensorflow.keras.layers import MaxPooling2D
from tensorflow.keras.layers import TimeDistributed
from tensorflow.keras.layers import concatenate, Reshape, Conv1D, MaxPooling1D
from pykalman import UnscentedKalmanFilter, KalmanFilter

import utils
from models import lstm, config
from models.layers.Time2Vec import Time2Vec
from models.layers.Weather2Vec import Weather2Vec


def create_cnn(n_lag, width, height, depth, filters=(100, 200, 300)):
    input_shape = (height, width, depth)
    channel_dim = -1

    inputs = Input(shape=input_shape)

    for (i, f) in enumerate(filters):
        if i == 0:
            x = inputs

        # CONV => RELU > BN => POOL
        x = Conv2D(f, (3, 3), padding="same")(x)
        x = Activation("relu")(x)
        x = BatchNormalization(axis=channel_dim)(x)
        x = MaxPooling2D(pool_size=(2, 2))(x)
    x = Flatten()(x)
    x = Dense(1024, activation="relu")(x)
    x = BatchNormalization(axis=channel_dim)(x)
    x = Dropout(0.2)(x)

    x = Dense(1, activation="linear")(x)

    model = Model(inputs, x)

    td_input = Input(shape=(n_lag, height, width, depth), name="images")

    td = TimeDistributed(model)(td_input)

    # inputs = tf.keras.Input(shape=(10, 128, 128, 3))
    # >> > conv_2d_layer = tf.keras.layers.Conv2D(64, (3, 3))
    # >> > outputs = tf.keras.log_result.TimeDistributed(conv_2d_layer)(inputs)

    td_model = Model(td_input, td)

    return td_model, td_input


def handle_lstm_cnn_skf(scaler, df, n_lag, n_epochs, n_outputs, test_size):
    start_ts = time.time()
    images = df['Image'].to_numpy()
    images = np.array(list(images), dtype=np.float64)
    images = images / 255.

    df.info()

    print(images[0].shape)

    (df_continious, df_continious_raw, df_cats) = preprocess(df)

    n_features = df_continious.shape[1]

    n_category = len(df_cats)  # number of categorical features
    print(f'n_category: {n_category}')
    print(f'n_features: {n_features}')

    numerical_input = Input(shape=(n_lag, n_features), name='continious_input')
    cat_inputs = []
    for i in range(n_category):
        cat_inputs.append(Input(shape=(n_lag, 1), name='categorical' + str(i + 1) + '_input'))

    cat_embedded = []
    for i in range(n_category):
        df_cat = df_cats[i]
        df_dim = len(np.unique(df_cat))
        embed = Embedding(len(df_cat), df_dim)(cat_inputs[i])
        cat_embedded.append(embed)


    time2vec_input = Input(shape=(n_lag, 1), name='time2vec')

    (model_cnn, model_cnn_input) = create_cnn(n_lag, 100, 300, 3)

    print(model_cnn.outputs)
    print(model_cnn.inputs)

    t = Time2Vec(n_lag)(time2vec_input)

    cat_merged = concatenate(cat_embedded)
    cat_merged = Reshape((n_lag, -1))(cat_merged)
    merged = concatenate([numerical_input, cat_merged, t] + model_cnn.outputs)

    x = lstm.create_lstm(merged, n_outputs)

    model = Model([numerical_input] + cat_inputs + [time2vec_input] + [model_cnn_input], x)
    model.summary()

    print("-> Constructing train/test split")

    print("-> Preprocessing")
    data = df_continious
    data_raw = df_continious_raw

    n_rows = data.shape[0]

    # Splitting into train and test sets
    train_raw = data_raw[0:int(n_rows * (1 - test_size))]
    test_raw = data_raw[int(n_rows * (1 - test_size)):]

    train = data[0:int(n_rows * (1 - test_size))]
    test = data[int(n_rows * (1 - test_size)):]

    # Creating the final scaled frame
    data_s = pd.concat([pd.DataFrame(train), pd.DataFrame(test)])
    data_s_raw = pd.concat([pd.DataFrame(train_raw), pd.DataFrame(test_raw)])
    data_images = images

    data_cats = []

    for i in range(n_category):
        train_cat = df_cats[i][0:int(n_rows * (1 - test_size))]
        test_cat = df_cats[i][int(n_rows * (1 - test_size)):]
        data_cats.append(pd.concat([pd.DataFrame(train_cat), pd.DataFrame(test_cat)]))


    timestamps = df['Datetime'].to_numpy()

    x, y = utils.timeseries_to_supervised(data_s.values, timestamps, lag=n_lag, n_ahead=n_outputs, target_index=0)
    x_raw, y_raw = utils.timeseries_to_supervised(data_s_raw.values, timestamps, lag=n_lag, n_ahead=n_outputs, target_index=0)
    x_cats = []
    y_cats = []

    for i in range(n_category):
        x_cat, y_cat = utils.timeseries_to_supervised(data_cats[i].values, timestamps, lag=n_lag, n_ahead=n_outputs,
                                                      target_index=0)
        x_cats.append(x_cat)
        y_cats.append(y_cat)

    x_hour, y_hour = utils.timeseries_to_supervised(data_s_raw.values[:, config.timeofday_index].reshape(-1, 1), timestamps, lag=n_lag,
                                                    n_ahead=n_outputs,
                                                    target_index=0)

    data_images = np.array([idx for idx, x in enumerate(data_images)]).reshape(-1, 1)

    x_images, y_images = utils.timeseries_to_supervised(data_images, timestamps, lag=n_lag, n_ahead=n_outputs, target_index=0)

    print(f'x: {x.shape}, y: {y.shape}')

    train_x, train_y = x[0:int(x.shape[0] * (1 - test_size))], y[0:int(x.shape[0] * (1 - test_size))]
    train_x_raw, train_y_raw = x_raw[0:int(x_raw.shape[0] * (1 - test_size))], y_raw[
                                                                               0:int(x.shape[0] * (1 - test_size))]
    test_x, test_y = x[int(x.shape[0] * (1 - test_size)):], y[int(x.shape[0] * (1 - test_size)):]
    test_x_raw, test_y_raw = x_raw[int(x_raw.shape[0] * (1 - test_size)):], y_raw[
                                                                            int(x_raw.shape[0] * (1 - test_size)):]

    cat_train_x, cat_train_y = x_cat[0:int(x_cat.shape[0] * (1 - test_size))], y_cat[
                                                                               0:int(x_cat.shape[0] * (1 - test_size))]
    cat_test_x, cat_test_y = x_cat[int(x_cat.shape[0] * (1 - test_size)):], y_cat[
                                                                            int(x_cat.shape[0] * (1 - test_size)):]

    cats_train_x = []
    cats_train_y = []
    cats_test_x = []
    cats_test_y = []

    for i in range(n_category):
        cat_train_x, cat_train_y = x_cats[i][0:int(x_cats[i].shape[0] * (1 - test_size))], y_cats[i][
                                                                                           0:int(x_cats[i].shape[0] * (
                                                                                                   1 - test_size))]
        cat_test_x, cat_test_y = x_cats[i][int(x_cats[i].shape[0] * (1 - test_size)):], y_cats[i][
                                                                                        int(x.shape[0] * (
                                                                                                1 - test_size)):]

        cats_train_x.append(cat_train_x)
        cats_train_y.append(cat_train_y)
        cats_test_x.append(cat_test_x)
        cats_test_y.append(cat_test_y)


    hour_train_x, hour_train_y = x_hour[0:int(x_hour.shape[0] * (1 - test_size))], y_hour[
                                                                                   0:int(x_hour.shape[0] * (
                                                                                           1 - test_size))]
    hour_test_x, hour_test_y = x_hour[int(x_hour.shape[0] * (1 - test_size)):], y_hour[
                                                                                int(x_hour.shape[0] * (1 - test_size)):]

    images_train_x, images_train_y = x_images[0:int(x_images.shape[0] * (1 - test_size))], y_images[
                                                                                           0:int(x_images.shape[0] * (
                                                                                                   1 - test_size))]
    images_test_x, images_test_y = x_images[int(x_images.shape[0] * (1 - test_size)):], y_images[int(
        x_images.shape[0] * (1 - test_size)):]

    print(f"Train on {len(images_train_x)}")
    print(f"Test on {len(images_test_x)}")

    def generator(total_batches, batch_size):
        idx = 0

        while True:
            start = idx * batch_size
            end = start + batch_size
            print(f'\nidx:{idx} yielding {start}:{end} for images {np.squeeze(images[images_train_x[start:end]]).shape}')
            print(
                f'\nidx:{idx} yielding {start}:{end} for images alt: {np.squeeze(images[images_train_x[start:end]], 2).shape}')

            yield {'continious_input': train_x[start:end],
                   'categorical1_input': cats_train_x[0][start:end],
                   'categorical2_input': cats_train_x[1][start:end],
                   'categorical3_input': cats_train_x[2][start:end],
                   'time2vec': hour_train_x[start:end],
                   'images': np.squeeze(images[images_train_x[start:end]], 2)}, train_y_raw[
                                                                              start:end]
            if idx < total_batches - 1:
                idx += 1
            else:
                idx = 0

    def test_generator(total_batches, batch_size):
        idx = 0

        while True:
            start = idx * batch_size
            end = start + batch_size

            print(f'\nidx:{idx} yielding test {start}:{end} for images {np.squeeze(images[images_test_x[start:end]]).shape}')
            print(f'\nidx:{idx} yielding test {start}:{end} for images alt: {np.squeeze(images[images_test_x[start:end]], 2).shape}')

            yield {'continious_input': test_x[start:end],
                   'categorical1_input': cats_test_x[0][start:end],
                   'categorical2_input': cats_test_x[1][start:end],
                   'categorical3_input': cats_test_x[2][start:end],
                   'time2vec': hour_test_x[start:end],
                   'images': np.squeeze(images[images_test_x[start:end]], 2)}, test_y_raw[start:end]
            if idx < total_batches - 1:
                idx += 1
            else:
                idx = 0

    print(f'Train: x: {train_x.shape}, y: {train_y.shape}')
    print(f'Test: x: {test_x.shape}, y: {test_y.shape}')

    print(f'Hour Train: x: {hour_train_x.shape}, y: {hour_train_y.shape}')
    print(f'Hour Test: x: {hour_test_x.shape}, y: {hour_test_y.shape}')

    print(f'Images Train: x: {images_train_x.shape}, y: {images_train_y.shape}')
    print(f'Images Test: x: {images_test_x.shape}, y: {images_test_y.shape}')

    model.compile(optimizer='adam', loss='mse', metrics=['mae'])

    # train the model

    batch_size = 2
    steps_per_epoch = np.ceil(train_x.shape[0] / batch_size)
    print(f"-> Training on {train_x.shape[0]} ({steps_per_epoch} batches)")

    history = model.fit(generator(steps_per_epoch, batch_size), verbose=1, epochs=n_epochs, steps_per_epoch=steps_per_epoch)

    print("-> Predicting noise levels")
    steps_per_epoch_test = np.ceil(test_x.shape[0] / batch_size)
    print(f"-> Testing on {test_x.shape[0]} ({steps_per_epoch_test} batches)")

    predictions = model.predict(test_generator(steps_per_epoch_test, batch_size), steps=steps_per_epoch_test)
    print(f'Predictions shape: {predictions.shape}')

    predictions_y = np.array([p[n_outputs - 1] for p in predictions])

    print("Predictions:")
    print(predictions_y[0:3])

    scaled_predictions_y = scaler.inverse_transform(predictions_y.reshape(-1, 1))
    print(f'Reshaped predictions shape: {scaled_predictions_y.shape}')
    scaled_test_y = scaler.inverse_transform(test_y_raw.reshape(-1, 1))
    print(f'Reshaped true shape: {scaled_test_y.shape}')

    print(f"True_scaled: {scaled_test_y.shape}")
    print(scaled_test_y[0:3])
    print(f"Predictions_scaled: {scaled_predictions_y.shape}")
    print(scaled_predictions_y[0:3])


    end_ts = time.time()
    utils.log_result('lstm_cnn_skf', history, n_lag, n_epochs, n_outputs,
                     scaled_test_y,
                     scaled_predictions_y, model, end_ts - start_ts)

    return scaled_predictions_y, scaled_test_y


def preprocess(df):
    target = config.target_variable

    continuous = config.continuous_variables

    target = df[target]

    scaler = MinMaxScaler(feature_range=(0, 1))
    continuous = scaler.fit_transform(df[continuous])

    cats = []
    categorical = config.categorical_variables

    for c in categorical:
        binarizer = LabelEncoder().fit(df[c])
        cat = binarizer.transform(df[c])
        cats.append(np.hstack([cat]))

    cont = np.hstack([target, continuous])

    cont_raw = np.hstack([target, continuous])

    kf = KalmanFilter(initial_state_mean=10, n_dim_obs=len(config.continuous_variables) + 1)

    print(f'Shape pre: {cont.shape}')

    cont = kf.em(cont[:, :].astype(np.float32)).smooth(cont[:, :].astype(np.float32))
    cont = np.array(cont[0])

    print(f'Shape post: {cont.shape}')

    return cont, cont_raw, cats
