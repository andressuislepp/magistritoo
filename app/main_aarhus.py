import argparse
import sys

import pandas as pd

import data_importer_aarhus

parser = argparse.ArgumentParser(description='ASDF - Noise prediction')

parser.add_argument('--n_epochs', type=int, help='Epochs for training')
parser.add_argument('--n_lag', type=int, help='Lag/look back for LSTM')
parser.add_argument('--n_outputs', type=int, help='Time steps to predict for LSTM')
parser.add_argument('--test_size', type=float, help='Test size for LSTM')
parser.add_argument('--method', help='Method',
                    choices=['load_data', 'lstm_uni', 'lstm_2vec_full_m4', 'lstm_2vec_full_m5', 'lstm_2vec_full_m6'])

from models.aarhus import lstm_2vec_full_m4, lstm_2vec_full_m5, lstm_2vec_full_m6, lstm_uni

def load():
    df = pd.read_pickle('aarhus.pkl')

    df.info()

    df.sort_values('Datetime', inplace=True)
    # Missing values handling
    # df = df.fillna(method='ffill', limit=2)
    # df = df.fillna(method='bfill', limit=2)
    df = df.loc[df['Minute'] % 5 == 0]
    # df = df.fillna(method='ffill', limit=4)

    return df


def import_data_to_pickle():
    traffic, pollution = data_importer_aarhus.import_all()

    df = pd.merge(traffic, pollution,
                  how='left',
                  on=['Date', 'Hour', 'Minute', 'Day_Of_Month', 'Day_Of_Week'])

    df.to_pickle('aarhus.pkl')
    return df


if __name__ == '__main__':

    print("<- startup")
    print("<- dataset: Aarhus")

    args = parser.parse_args()

    print(f'<- method {args.method}')
    print(f'<- n_epochs {args.n_epochs}')
    print(f'<- n_lag {args.n_lag}')
    print(f'<- n_outputs {args.n_outputs}')
    print(f'<- test_size {args.test_size}')

    if args.method == 'load_data':
        # --- LOAD NUMERIC/CATEGORICAL DATA TO PICKLE
        print("<- loading data to pickle")
        df = import_data_to_pickle()
        print("<- loading data to pickle finished")
        df.info()
        sys.exit(-1)
    if args.method == 'lstm_2vec_full_m4':
        df = load()
        df.info()

        lstm_2vec_full_m4.handle_lstm_2vec(df, args.n_lag, args.n_epochs, args.n_outputs, args.test_size)
        sys.exit(-1)
    if args.method == 'lstm_2vec_full_m5':
        df = load()
        df.info()

        lstm_2vec_full_m5.handle_lstm_2vec(df, args.n_lag, args.n_epochs, args.n_outputs, args.test_size)
        sys.exit(-1)
    if args.method == 'lstm_2vec_full_m6':
        df = load()
        df.info()

        lstm_2vec_full_m6.handle_lstm_2vec(df, args.n_lag, args.n_epochs, args.n_outputs, args.test_size)
        sys.exit(-1)
    if args.method == 'lstm_uni':
        df = load()
        df.info()

        lstm_uni.handle_lstm_2vec(df, args.n_lag, args.n_epochs, args.n_outputs, args.test_size)
        sys.exit(-1)

