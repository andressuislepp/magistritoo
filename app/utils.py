import matplotlib.pyplot as plt
import numpy as np
from sklearn.metrics import mean_squared_error, mean_absolute_error, mean_absolute_percentage_error

def timeseries_to_supervised(ts: np.array, timestamps: np.array, lag=1, n_ahead=1, target_index=0) -> tuple:
    """
    A method to create X and Y matrix from a time series array
    """
    # Extracting the number of features that are passed from the array
    n_features = ts.shape[1]

    # Creating placeholder lists
    x, y = [], []

    if len(ts) - lag <= 0:
        x.append(ts)
    else:
        for i in range(len(ts) - lag - n_ahead):
            # check that whole train set is available for the "lag" time
            tims = timestamps[i:(i + lag)]

            first = tims[0].astype(object) / 1000 / 1000 / 1000
            last = tims[lag - 1].astype(object) / 1000 / 1000 / 1000

            diff_minutes = (last - first) / 60

            if diff_minutes <= ((lag + 0.1) * 5):
                y.append(ts[(i + lag + n_ahead), target_index])
                x.append(ts[i:(i + lag)])

    x, y = np.array(x), np.array(y)

    # Reshaping the X array to an RNN input shape
    x = np.reshape(x, (x.shape[0], lag, n_features))

    return x, y

def evaluate(y_true, y_predictions):
    mse = mean_squared_error(y_true, y_predictions)
    rmse = np.sqrt(mse)
    mae = mean_absolute_error(y_true, y_predictions)
    mape = mean_absolute_percentage_error(y_true, y_predictions)

    return mse, rmse, mae, mape


def log_result(method, history, n_lag, n_epochs, n_outputs, scaled_test_y, scaled_predictions_y, model, time_diff):
    name = f'lag{n_lag}_epochs{n_epochs}_{method}_{n_outputs}'

    n_params = '-'
    n_trainable_params = '-'

    if model is not None:
        model.save(f'output/{name}')
        summary_rows = []
        model.summary(print_fn=lambda x: summary_rows.append(x))
        for row in summary_rows:
            if "Total params:" in row:
                n_params = row.split(': ')[1]
            if "Trainable params:" in row:
                n_trainable_params = row.split(': ')[1]

    mse, rmse, mae, mape = evaluate(scaled_test_y, scaled_predictions_y)

    res = '\t'.join(map(str, [method.ljust(20, ' '), n_lag, n_epochs, n_outputs, '%.3f' % rmse, '%.3f' % mse, '%.3f' % mae, '%.3f' % mape, time_diff, n_params, n_trainable_params]))
    print(res)

    plt.figure(name)
    plt.plot(scaled_test_y, label='Noise')
    plt.plot(scaled_predictions_y, label='Model')

    plt.legend()
    plt.savefig(f'output/{name}.eps', format='eps')

    if history is not None:
        try:
            plt.figure(f'{name}-training')
            plt.plot(history.history['loss'])
            plt.plot(history.history['val_loss'])
            plt.title('Model Loss')
            plt.ylabel('Loss')
            plt.xlabel('Epoch')
            plt.legend(['Training', 'Validation'], loc='upper right')
            plt.savefig(f'output/{name}-training.eps', format='eps')
        except:
            print("error saving training plot")

    with open("output/results.tsv", "a") as file:
        file.write(res)
        file.write('\n')

def log_regression_result(method, n_epochs, mape):
    res = '\t'.join(map(str, [method, n_epochs, '%.3f' % mape]))
    print(res)

    with open("regression_results.tsv", "a") as file:
        file.write(res)
        file.write('\n')
