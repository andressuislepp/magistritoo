import argparse
import sys

import cv2
import matplotlib.pyplot as plt
import pandas as pd
import tensorflow as tf

import data_importer
import image_importer
from sklearn.preprocessing import MinMaxScaler
import seaborn as sns

from datetime import datetime

from models.discarded import lstm_vgg16_ukf
from models.regression import cnn_cameras, mixed_cnnmlp, mlp

parser = argparse.ArgumentParser(description='ASDF - Noise prediction')

parser.add_argument('--n_epochs', type=int, help='Epochs for training')
parser.add_argument('--n_lag', type=int, help='Lag/look back for LSTM')
parser.add_argument('--n_outputs', type=int, help='Time steps to predict for LSTM')
parser.add_argument('--test_size', type=float, help='Test size for LSTM')
parser.add_argument('--method', help='Method',
                    choices=['process', 'load_data', 'load_images', 'lstm_img_cnn', 'lstm_df_full', 'lstm_univariate', 'lstm_nofusion', 'lstm_fusion_kf', 'lstm_img_vgg16', 'lstm_img_vgg16_md', 'lstm_cnn_ukf', 'lstm_vgg16_ukf', 'lstm_vgg16_ukf_md', 'lstm_fusion_kf_2vec', 'lstm_fusion_ukf', 'lstm_fusion_ukf_2vec', 'lstm_fusion_skf', 'lstm_fusion_skf_2vec', 'lstm_cnn', 'naive', 'moving_average', 'lstm_vgg16'])


tf.keras.utils.set_random_seed(
    200
)

def load():
    df, target_scaler = load_with_images('sm')
    df.drop('Image', axis=1)

    df.info()

    return df, target_scaler


def load_with_images(size):
    if size == 'md':
        df = pd.read_pickle('10feb-06mar-cameras-md.pkl')
        df.drop('Image_x', axis=1)
        df = df.rename(columns={'Image_y': 'Image'})
    else:
        df = pd.read_pickle('10feb-06mar-cameras-sm.pkl')
    df.sort_values('Datetime', inplace=True)

    # df['TN_Noise_Level_DB'] = df['TN_Noise_Level_DB'].fillna(method='bfill')

    df = df.fillna(method='bfill', limit=12)
    df = df.loc[df['Minute'] % 5 == 0]
    df = df.fillna(method='bfill', limit=2)

    df.info()
    # df = df.head(250)

    df = df.loc[df['TN_Noise_Level_DB'].notnull()]
    df = df.loc[df['TT_P1_Speed_Diff_Kmh'].notnull()]
    df = df.loc[df['TT_P1_Travel_Time_Diff_Sec'].notnull()]
    df = df.loc[df['TT_P2_Speed_Diff_Kmh'].notnull()]
    df = df.loc[df['TT_P2_Travel_Time_Diff_Sec'].notnull()]
    df = df.loc[df['IW_Temp_C'].notnull()]
    df = df.loc[df['IW_Wind_Temp_C'].notnull()]
    df = df.loc[df['IW_Rain_MMH'].notnull()]
    df = df.loc[df['Image'].notnull()]
    df = df.loc[df['IW_Wind_Speed_MH'].notnull()]
    df.sort_values('Datetime', inplace=True)

    target = ['TN_Noise_Level_DB']
    target_scaler = MinMaxScaler(feature_range=(0, 1))
    df['TN_Noise_Level_DB'] = target_scaler.fit_transform(df[target])

    return df, target_scaler


def import_data_to_pickle():
    tallinn_noise, tallinn_weather, ilmee_weather, tomtom_traffic_p1, tomtom_traffic_p2 = data_importer.import_all()

    df = pd.merge(tallinn_weather, tallinn_noise,
                  how='left',
                  on=['Date', 'Hour', 'Minute', 'Day_Of_Month', 'Day_Of_Week'])

    df = pd.merge(df, ilmee_weather,
                  how='left',
                  on=['Date', 'Hour', 'Minute', 'Day_Of_Month', 'Day_Of_Week'])

    df = pd.merge(df, tomtom_traffic_p1,
                  how='left',
                  on=['Date', 'Hour', 'Minute', 'Day_Of_Month', 'Day_Of_Week'])

    df = pd.merge(df, tomtom_traffic_p2,
                  how='left',
                  on=['Date', 'Hour', 'Minute', 'Day_Of_Month', 'Day_Of_Week'])

    df.to_pickle('fe10mar18.pkl')
    return df


def load_image_data(df):
    images = image_importer.import_all()

    df = pd.merge(df, images,
                  how='left',
                  on=['Date', 'Hour', 'Minute'])

    df.to_pickle('10feb-06mar-cameras-md.pkl')

    df.info()


if __name__ == '__main__':

    print("<- startup")
    print("<- dataset: 10 Feb - 6 March")


    df = pd.read_pickle('10feb-06mar-cameras-sm.pkl')
    df['Minute_Of_Day'] = df.apply(lambda row: (row['Hour'] * 60) + row['Minute'], axis=1)

    print(df['Minute_Of_Day'])
    df.to_pickle('10feb-06mar-cameras-sm.pkl')

