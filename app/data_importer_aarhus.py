import json
from datetime import datetime

import pandas as pd
from bs4 import BeautifulSoup


def compute_datetime_features(df, drop_ts=True):
    df['Datetime'] = df.apply(lambda row: datetime.fromisoformat(row['Timestamp']), axis=1)
    df['Date'] = df.apply(lambda row: row['Datetime'].strftime("%Y-%m-%d"), axis=1)
    df['Hour'] = df.apply(lambda row: row['Datetime'].hour, axis=1)
    df['Minute'] = df.apply(lambda row: row['Datetime'].minute, axis=1)
    df['Day_Of_Month'] = df.apply(lambda row: row['Datetime'].day, axis=1)
    df['Day_Of_Week'] = df.apply(lambda row: row['Datetime'].weekday(), axis=1)
    # TODO is public holiday?

    if drop_ts:
        df = df.drop("Timestamp", axis=1)
        df = df.drop("Datetime", axis=1)

    return df


def import_aarhus_pollution():
    df = pd.read_csv('data/aarhus/pollutionData158595.csv',
                     skiprows=1,
                     names=["Ozone", "ParticullateMatter", "CarbonMonoxide", "SulfurDioxide", "NitrogenDioxide", "Longitude", "Latitude", "Timestamp"])
    df = compute_datetime_features(df)

    return df


def import_aarhus_traffic():
    df = pd.read_csv('data/aarhus/trafficData158595.csv',
                     skiprows=1,
                     names=["Status", "AvgTime", "AvgSpeed", "ExtID", "MedianTime", "Timestamp", "VehicleCount", "_id", "ReportId"])

    df.drop("Status", axis=1)
    df.drop("_id", axis=1)
    df.drop("ReportId", axis=1)
    df = compute_datetime_features(df, False)

    return df


def compose_safe_fn(fn):
    def safe_fn(row):
        try:
            return fn(row)
        except:
            return None

    return safe_fn


def import_all():
    pollution = import_aarhus_pollution()
    traffic = import_aarhus_traffic()

    return traffic, pollution
