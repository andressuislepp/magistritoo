
# MULTI-MODAL DATA FUSION FOR SHORT-TERM URBAN NOISE PREDICTION IN INTELLIGENT TRANSPORTATION SYSTEMS

Author: **Andres Suislepp**

## Structure
```bash
app/data % raw input data
app/models % data fusion models with LSTM time series predictions
app/models/layers % custom implementations of neural network layers
app/models/timeseries % statistical time series method implementations
app/models/config.py % global configurations used by all models
app/models/lstm.py % global lstm configurations used by all deep learning models
app/output % directory where results are written and models are saved. **must be created before running the application**
app/10feb-06mar-cameras-sm.pkl % dataset in pickle format

```
## Requirements
Requirements are defined in ```requirements.txt```.

## Usage
```bash
usage: app/main.py [-h] [--n_epochs N_EPOCHS] [--n_lag N_LAG]
               [--n_outputs N_OUTPUTS] [--test_size TEST_SIZE]
               [--method {process,load_data,load_images,arima,linear_regression,lstm_img_cnn,lstm_df_full,lstm_univariate,lstm_nofusion,lstm_fusion_kf,lstm_img_vgg16,lstm_img_vgg16_md,lstm_cnn_ukf,lstm_vgg16_ukf,lstm_vgg16_ukf_md,lstm_fusion_kf_2vec,lstm_fusion_ukf,lstm_fusion_ukf_2vec,lstm_fusion_skf,lstm_fusion_skf_2vec,lstm_cnn,naive,moving_average,lstm_vgg16}]
```
### Parameters
| Name        | Description |
| ----------- | ----------- |
| n_epochs    | How many epochs to train the model       |
| n_lag   | input sequence length for time series models        |
| n_outputs   | how many time steps into the future to predict        |
| test_size   | how much data to use for model validation in decimals (0.20 = 20%)        |
| method   | which data fusion method and prediction model to use         |


### Models & methods
| Name        | Description | Implementation | 
| ----------- | ----------- | ----------- |
| lstm_cnn_skf      | Images as input to model. No decision fusion. Smoothed kalman filter feature fusion.        | ```app/models/lstm_cnn_skf.py``` |
| lstm_df_full      | Runner for all models and decision fusion implementations.       | ```app/models/lstm_df_full.py``` |
| lstm_fusion_kf_2vec      | No image inputs. All variables included and Kalman Filter fusion for continuous variables.       | ```app/models/lstm_kf_2vec.py``` |
| lstm_fusion_kf_2vec      | No image inputs. All variables included and Smoothed Kalman Filter fusion for continuous variables.       | ```app/models/lstm_skf_2vec.py``` |
| lstm_fusion_kf_2vec      | No image inputs. All variables included and Uncented Kalman Filter fusion for continuous variables.       | ```app/models/lstm_ukf_2vec.py``` |
| lstm_img_cnn      | Only images, no fusion with other variables       | ```app/models/lstm_img_cnn.py``` |
| lstm_nofusion      | No image inputs and no fusion strategies. All parameters as input to the prediction model.       | ```app/models/lstm_nofusion.py``` |
| lstm_univariate      | Univariate prediction model only based on the target variable noise.       | ```app/models/lstm_univariate.py``` |
| arima      | ARIMA prediction model.       | ```app/models/timeseries/arima.py``` |
| linear_regression      | Linear regression prediction model.       | ```app/models/timeseries/linear_regression.py``` |
| moving_average      | Moving average prediction model.       | ```app/models/timeseries/moving_average.py``` |
| naive      | Naive prediction model.       | ```app/models/timeseries/naive.py``` |


## Dataset
Dataset is saved in pickle format at ```app/10feb-06mar-cameras-sm.pkl```. To investigate the dataset using Pandas dataframe:
```bash
df = pd.read_pickle('10feb-06mar-cameras-sm.pkl')
df.info()
df.describe()
```